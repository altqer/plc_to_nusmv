# PLC-NuSMV compiler #

This is the repository for the PLC-NuSMV compiler. This program can compile PLC
programs in the FBD language to the input language of NuSMV. It takes a PLC
program and a truth table file as input and outputs a NuSMV program that can be
verified by the NuSMV model checker.

The main goal of the PLC-NuSMV compiler is to achieve automation in translation
of PLC code to the input language of a model checker. The PLC programmers that
need to verify PLC programs will save time and effort by having a tool that does
this automatically.

## Supported PLC programs ##
There are some limitations to what kind of PLC programs the compiler can parse.
These are:

* PLC programs have to be saved in XML documents that follows the PLCopen XML
schema. Visit the [PLCopen website](http://www.plcopen.org/) for more
information
* Only the PLC language Function Block Diagram (FBD) is supported
* Only one POU of type "Program" are currently allowed
* All the output variables of the PLC program should be Boolean functions. The
PLC program should basically be a set of pure Boolean functions/formulas.
* Feedback loops in the FBD graph are not allowed

## Other limitations ##
The truth table is the specification of the PLC program. The PLC-NuSMV compiler
parses the truth table file and creates a NuSMV module and an LTL specification
in the main module of the NuSMV program based on the truth table. This type of
specification limits the compiler to only small PLC programs. Support for using
Boolean formulas as specifications should be the main priority for further
development of the program.

## How to run ##
The program is a command-line program. To compile a PLC program you give it the
path of the XML file where the program is saved in and the path of the truth
table file. As default, the program outputs the NuSMV program to the standard
output. You can redirect the output to a file yourself or pass the NuSMV file
you want it to generate with the "--nusmv" command-line option.

The program can also generate PLC programs with the "--test" command-line
options. This is used for testing purposes, hence the name of the option. This
command-line option will ignore the xml file and truth table file if given as
parameters.

### Command-line options ###
The PLC-NuSMV compiler takes the following command-line options/parameters:

#### --xml (-x) ####
The XML file where the PLC program is saved in.

#### --truth-table (-t) ####
The truth table file that will be used as a specifacation to the program. It is
basically just a file with the variable names in the first line and the truth
table values in the following lines. ALl variables and values should be
separated by commas, semicolons or any other non-alphanumerical characters.
                
#### --nusmv (-n) ####
The path of the NUSMV file to generate.

#### --schema ####
The XML schema file that is used by the compiler. Only the PLC open XML schema
2.01 can be used currently. This XML schema is included with the program.

#### --test ####
Generate a PLC program with a given number of input and output variables. There
are restrictions on the number of variables and these are x >= 2, y >= 1 and
x > y, where x and y are the number of input and output variables respectively.

#### --optimize (-o) ####
Optimize the Truth table module by building and using Boolean formulas instead
of the actual truth table. It skips the parsing of the truth table file.

#### --help ####
Prints information about all or just one specific command-line option.

## How do I get set up? ##
The program uses a couple of Qt modules, mainly for XML document parsing. You
therefore need to have the Qt framework to be able to compile the program.

The PLC-NuSMV compiler is cross-platform and has been tested in Linux and
Windows.

## Contact information ##

* Repo owner and main contributor: Altin Qeriqi (altin89@gmail.com)

## License notice ##
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
