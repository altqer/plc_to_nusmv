#QT += core
QT -= gui
QT += xmlpatterns

CONFIG += c++11
CONFIG += static

TARGET = plc_to_nusmv
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += plc_to_nusmv/main.cpp \
    plc_to_nusmv/pou.cpp \
    plc_to_nusmv/plcvariable.cpp \
    plc_to_nusmv/node.cpp \
    plc_to_nusmv/plctonusmv.cpp \
    plc_to_nusmv/edge.cpp \
    plc_to_nusmv/fbdgraph.cpp \
    ast/module.cpp \
    ast/program.cpp \
    ast/decl_param.cpp \
    ast/decl.cpp \
    ast/decl_var.cpp \
    ast/decl_define.cpp \
    ast/expr_var.cpp \
    ast/expr_literal.cpp \
    ast/expr_op.cpp \
    ast/decl_assign.cpp \
    ast/expr_case.cpp \
    ast/type_specifier.cpp \
    ast/expr_set.cpp \
    ast/ltl_spec.cpp \
    plc_to_nusmv/astbuilder.cpp \
    plc_to_nusmv/testcasegenerator.cpp \
    plc_to_nusmv/plcreader.cpp \
    plc_to_nusmv/truthtablereader.cpp \
    plc_to_nusmv/plctonusmvlogger.cpp \
    plc_to_nusmv/nusmvcodegenerator.cpp

HEADERS += \
    plc_to_nusmv/pou.h \
    plc_to_nusmv/plcvariable.h \
    plc_to_nusmv/node.h \
    plc_to_nusmv/plctonusmv.h \
    plc_to_nusmv/edge.h \
    plc_to_nusmv/fbdgraph.h \
    ast/astnode.h \
    plc_to_nusmv/astbuilder.h \
    plc_to_nusmv/testcasegenerator.h \
    plc_to_nusmv/plcreader.h \
    plc_to_nusmv/truthtablereader.h \
    plc_to_nusmv/excepts.h \
    plc_to_nusmv/plctonusmvlogger.h \
    plc_to_nusmv/nusmvcodegenerator.h \
    ast/astnodevisitor.h

unix {
    QMAKE_LFLAGS += -Wl,-rpath,"'\$$ORIGIN/qtlibs'"
    message("For deployment in Unix operating systems, copy the necessary\
        Qt shared object files to the directory $$OUT_PWD/qtlibs. You can get\
        the name of these files by using the ldd program.")
    #message(4 $$IN_PWD)
}

# Copying the XML schema file to install dir
#Debug|Release {
debug|release {
    xsd_copy.commands = $(COPY_DIR) \"$$PWD/xsd\" \"$$OUT_PWD/xsd\"
    XSD_DIR_DEST = $$OUT_PWD/xsd
    win32 {
        Release {
            xsd_copy.commands = $(COPY_DIR) \"$$PWD/xsd\" \"$$OUT_PWD/release/xsd\"
            XSD_DIR_DEST = $$OUT_PWD/release/xsd
        }
        Debug {
            xsd_copy.commands = $(COPY_DIR) \"$$PWD/xsd\" \"$$OUT_PWD/debug/xsd\"
            XSD_DIR_DEST = $$OUT_PWD/debug/xsd
        }
    }

    message(Copying XML schema file to \"$${XSD_DIR_DEST}\")
    first.depends = $(first) xsd_copy
    export(first.depends)
    export(xsd_copy.commands)
    QMAKE_EXTRA_TARGETS += first xsd_copy
}

win32|macx {
    isEmpty(TARGET_EXT) {
        win32 {
            TARGET_CUSTOM_EXT = .exe
        }
        macx {
            TARGET_CUSTOM_EXT = .app
        }
    } else {
        TARGET_CUSTOM_EXT = $${TARGET_EXT}
    }

    win32 {
        DEPLOY_COMMAND = windeployqt
    }
    macx {
        DEPLOY_COMMAND = macdeployqt
    }

    CONFIG( debug, debug|release ) {
        # debug
        DEPLOY_TARGET = $$shell_quote($$shell_path($${OUT_PWD}/debug/$${TARGET}$${TARGET_CUSTOM_EXT}))
    } else {
        # release
        DEPLOY_TARGET = $$shell_quote($$shell_path($${OUT_PWD}/release/$${TARGET}$${TARGET_CUSTOM_EXT}))
    }

    #  # Uncomment the following line to help debug the deploy command when running qmake
    #  warning($${DEPLOY_COMMAND} $${DEPLOY_TARGET})

    QMAKE_POST_LINK += $${DEPLOY_COMMAND} $${DEPLOY_TARGET} &

}

# Deleting unnecessary files after running the previous deployment command
Debug:win32-msvc*|Release:win32-msvc* {
    Release:TARGET_DIR = $$OUT_PWD/release/
    Debug:TARGET_DIR = $$OUT_PWD/debug/

    DELETE_FILES = vcredist_x64.exe translations bearer
    for (f, DELETE_FILES) {
        DEL_FILE = $${TARGET_DIR}$$f
        DEL_FILE ~= s,/,\\,g
        message(Deleting \"$$DEL_FILE\")
        QMAKE_POST_LINK += del /Q \"$$DEL_FILE\" &
        # Use rmdir /S /Q instead?
    }

    DELETE_DIRS = translations bearer
    for (d, DELETE_DIRS) {
        DEL_FILE = $${TARGET_DIR}$$d
        DEL_FILE ~= s,/,\\,g
        message(Deleting directory \"$$DEL_FILE\")
        QMAKE_POST_LINK += rd /Q /S \"$$DEL_FILE\" &
    }
}

# Copying needed MSVC files
Release:win32-msvc* {
    MSVC_VER = $$(VisualStudioVersion)
    equals(MSVC_VER, 10.0){
        message("MSVC 2010 detected")
        MSVC_DLLS = msvcp100.dll msvcr100.dll
        COPY_DLLS = yes
    }

    equals(MSVC_VER, 11.0){
        message("MSVC 2012 detected")
        MSVC_DLLS = msvcp110.dll msvcr110.dll
        COPY_DLLS = yes
    }

    equals(MSVC_VER, 12.0){
        message("MSVC 2013 detected")
        MSVC_DLLS = msvcp120.dll msvcr120.dll
        COPY_DLLS = yes
    }

    for (a, MSVC_DLLS) {
        ! exists ($(SYSTEMROOT)/System32/$$a) {
            message($$a "doesn't exist")
            COPY_DLLS = no
        }
    }

    equals (COPY_DLLS, "yes") {
        COPY_DEST = $$OUT_PWD/release
        COPY_DEST ~=  s,/,\\,g
        for (a, MSVC_DLLS) {
            # Sysnative instead of System32 will copy the correct version
            # using the 32 bit cmd.exe. Should change this to
            # $(SYSTEMROOT)/System32/$$a if Qt is updated to run 64 bit cmd.exe.
            DLL_FILE = $(SYSTEMROOT)/Sysnative/$$a
            DLL_FILE ~=  s,/,\\,g
            message(Copying \"$$DLL_FILE\" to \"$$COPY_DEST\")
            QMAKE_POST_LINK += $(COPY_FILE) \"$$DLL_FILE\" \"$${COPY_DEST}\" &
        }
    }
}
