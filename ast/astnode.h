/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef ASTNODE_H
#define ASTNODE_H

#include <string>
#include <vector>
#include <unordered_set>
#include <sstream>

#include "astnodevisitor.h"

namespace nusmvast {

class Module;
class Decl;
class ParamDecl;
class VarDecl;
class DefineDecl;
class AssignDecl;
class TypeSpecifier;
class Expression;
class VarExpr;
class LTLExpr;

class AstNodeVisitor;

/*
 * The base class for all AST node classes.
 */
class ASTNode
{
public:
    virtual ~ASTNode() {}

    /*
     * Visualizes and prints the AST in a taxtual form. The method should only
     * be used for debugging purposes.
     */
    virtual void print_ast(const std::string & ws) const = 0;

    // The accept method for the base Visitor class for traversing the AST.
    virtual void accept(AstNodeVisitor & v) const = 0;
};

/*
 * A node that represents the entire NuSMV program.
 */
class Program : public ASTNode {
public:
    Program() {}
    Program(std::vector<Module *> & module_list);
    virtual ~Program();

    const std::vector<Module *> & get_module_list() const
        { return m_module_list; }

    // Inserts a module in the program.
    bool insert_module(Module * m);

    virtual void print_ast(const std::string & ws) const;
    virtual void accept(AstNodeVisitor & v) const;

private:
    std::vector<Module *> m_module_list;
    std::unordered_set<std::string> symtable;
};

/*
 * The class represents a module in NuSMV.
 */
class Module : public ASTNode {
public:
    Module(const std::string & name) : m_name(name) {}
    Module(const std::string & name, const std::vector<ParamDecl> & params);

    // Returns the name of the module.
    std::string get_name() const { return m_name; }

    /*
     * Returns an object that contains the name of the module and list of
     * module parameters.
     */
    TypeSpecifier get_type_specifier() const;

    // Returns the list of parameters for the module.
    std::vector<ParamDecl> get_params() const { return m_params; }

    // Returns a list of VAR declerations. See NuSMV manual for more
    // information.
    std::vector<VarDecl> get_vars() const { return m_vars; }

    // Returns a list of DEFINE declaration.
    std::vector<DefineDecl> get_defines() const { return m_defines; }

    // Returns a list of ASSIGN declaration.
    std::vector<AssignDecl> get_assigns() const { return m_assign_constraints; }

    // Returns a list of LTL specifications
    std::vector<LTLExpr> get_ltl_specs() const { return m_ltl_specs; }

    bool insert_param(const ParamDecl & pd);
    bool insert_var(const VarDecl & vd);
    bool insert_define(const DefineDecl & dd);
    bool insert_assign(const AssignDecl & ad);
    bool insert_ltl(const LTLExpr & ltl);

    virtual void print_ast(const std::string & ws) const;
    virtual void accept(AstNodeVisitor & v) const;
private:
    std::string m_name;
    std::vector<ParamDecl> m_params;
    std::vector<VarDecl> m_vars;
    std::vector<DefineDecl> m_defines;
    std::vector<AssignDecl> m_assign_constraints;
    std::vector<LTLExpr> m_ltl_specs;
    std::unordered_set<std::string> symtable;
};


/******************************
 * Declarations
 *****************************/

/*
 * Base class for all types of declaration.
 */
class Decl : public ASTNode {
public:
    Decl() {}
    Decl(const Module & parent_module);
    virtual ~Decl();

    virtual void print_ast(const std::string & ws) const = 0;
    virtual void accept(AstNodeVisitor & v) const = 0;
private:
    Module * m_parent_module;
    bool m_parent_set = false;
};

/*
 * A node that represents a parameter in a module. Only a parameter name is
 * needed to instantiate the class.
 */
class ParamDecl : public Decl {
public:
    ParamDecl(const std::string & name) : m_name(name) {}

    std::string get_name() const { return m_name; }

    virtual void print_ast(const std::string & ws) const;
    virtual void accept(AstNodeVisitor & v) const;
private:
    std::string m_name;
};

/*
 * A node class that represents a state variable declaration. A variable
 * declaration is placed in the VAR section of a module.
 */
class VarDecl : public Decl {
public:
    VarDecl(const std::string & name, const TypeSpecifier & type);
    VarDecl(const VarDecl & var);
    virtual ~VarDecl();

    std::string get_name() const { return m_name; }
    std::string get_type() const;
    const TypeSpecifier * get_type_specifier() const { return m_type; }

    virtual void print_ast(const std::string & ws) const;
    virtual void accept(AstNodeVisitor & v) const;
private:
    std::string m_name;
    TypeSpecifier * m_type;
};

/*
 * A node class that represents a variable declaration. A variable declaration
 * is placed in the VAR section of a module.
 */
class DefineDecl : public Decl {
public:
    DefineDecl(const std::string & identifier, const Expression * expr);
    DefineDecl(const DefineDecl & decl);
    virtual ~DefineDecl();

    std::string get_identifier() const { return m_identifier; }
    const Expression * get_expr() const { return m_expr; }

    virtual void print_ast(const std::string & ws) const;
    virtual void accept(AstNodeVisitor & v) const;
private:
    std::string m_identifier;
    Expression * m_expr;
};

/*
 * A node class that represents an assign declaration. State variables are
 * assigned values in the ASSIGN section of a NuSMV module. They are also
 * known as assign_constraint from the NUSMV grammar in the NuSMV manual.
 *
 * There are three types of assigns: standard, init and next.
 */
class AssignDecl : public Decl {
public:
    enum assign_type_enum {STANDARD, INIT, NEXT};

    AssignDecl(assign_type_enum type, const VarExpr & var,
               const Expression & expr);
    AssignDecl(const AssignDecl & decl);
    virtual ~AssignDecl();

    assign_type_enum get_assign_type() const { return m_assign_type; }
    const VarExpr * get_var() const { return m_var; }
    const Expression * get_expr() const { return m_expr; }

    virtual void print_ast(const std::string & ws) const;
    virtual void accept(AstNodeVisitor & v) const;
private:
    assign_type_enum m_assign_type;
    VarExpr * m_var; // complex_identifier
    Expression * m_expr;
};

/*
 * A node class that contains the name of the module and a list of module
 * parameters for the same module. It's used for representing an instantiation
 * of the module.
 */
class TypeSpecifier : public ASTNode {
public:
    TypeSpecifier(std::string type) : m_type(type) {}
    TypeSpecifier(std::string type, std::vector<Expression *> & params);
    TypeSpecifier(const TypeSpecifier & ts);
    virtual ~TypeSpecifier();

    std::string get_type() const { return m_type; }
    std::vector<Expression *> get_params() const { return m_params; }
    void insert_param_expr(const Expression * expr);

    virtual void print_ast(const std::string & ws) const;
    virtual void accept(AstNodeVisitor & v) const;
private:
    std::string m_type;
    std::vector<Expression *> m_params;
};

/******************************
 * Expressions
 *****************************/

/*
 * The base class for all expression AST nodes.
 */
class Expression : public ASTNode {
public:
    Expression() {}
    virtual ~Expression() {}
    virtual Expression* clone() const = 0;

    virtual void print_ast(const std::string &) const = 0;
    virtual void accept(AstNodeVisitor & v) const = 0;
};

/*
 * An expression node that represents either a simple variable reference or a
 * complex identifier where you reference a variable in a module instatiation,
 * e.g., fbd.x, where fbd is the module instance name and x is a state variable
 * in the module.
 *
 * Also known as complex_identifier from the NUSMV grammar
 */
class VarExpr : public Expression {
public:
    VarExpr(std::string identifier) : m_identifier(identifier) {}
    VarExpr(std::string identifier, const VarExpr & expr);
    VarExpr(const VarExpr & expr);
    virtual ~VarExpr();
    virtual VarExpr* clone() const { return new VarExpr(*this); }

    std::string get_identifier() const { return m_identifier; }
    const VarExpr * get_var() const { return m_expr; }
    bool is_expr_set() const  { return m_expr_set; }

    virtual void print_ast(const std::string & ws) const;
    virtual void accept(AstNodeVisitor & v)  const;
private:
    std::string m_identifier;
    VarExpr * m_expr;
    bool m_expr_set = false;
};

/*
 * A AST node that represents a literal value like, FALSE and TRUE.
 */
class LiteralExpr : public Expression {
public:
    LiteralExpr(const std::string & type, const std::string & literal)
        : m_type(type), m_literal(literal) {}
    virtual LiteralExpr * clone() const { return new LiteralExpr(*this); }

    virtual std::string get_type() const { return m_type; }
    // Returns the actual string literal
    std::string get_literal() const { return m_literal; }

    virtual void print_ast(const std::string & ws) const;
    virtual void accept(AstNodeVisitor & v)  const;
private:
    std::string m_type;
    std::string m_literal;
};

/*
 * A node class that represents an expressions that has an operator. Several
 * Boolean operators with two operands and one operands are supported. These
 * are: and, or, xor and not.
 */
class OpExpr : public Expression {
public:
    OpExpr(const std::string & type, const std::string & symbol,
               const Expression & left);
    OpExpr(const std::string & type, const std::string & symbol,
           const Expression & left, const Expression & right);
    OpExpr(const OpExpr & expr);
    virtual ~OpExpr();
    virtual OpExpr * clone() const { return new OpExpr(*this); }

    const Expression * get_left_expression() const { return m_left; }
    const Expression * get_right_expression() const { return m_right; }
    std::string get_type() const { return m_type; }
    std::string get_symbol() const { return m_symbol; }

    virtual void print_ast(const std::string & ws) const;
    virtual void accept(AstNodeVisitor & v) const;
private:
    std::string m_type;
    std::string m_symbol;
    Expression * m_left = nullptr;
    Expression * m_right = nullptr;
};

/*
 * Case expressions are switch-case expressions that can assign values to a
 * variable.
 */
class CaseExpr : public Expression {
public:
    CaseExpr() {}
    CaseExpr(const std::vector<std::vector<bool> > & truth_table,
             const std::vector<std::string> & truth_table_vars,
             bool truth_table_value);
    CaseExpr(const CaseExpr &);
    virtual ~CaseExpr();
    virtual CaseExpr * clone() const { return new CaseExpr(*this); }


    std::vector<std::pair<Expression *, Expression *>> get_cases() const {
        return m_cases;
    }
    const std::vector<std::vector<bool> > & get_truth_table() const
        { return m_truth_table; }
    const std::vector<std::string> & get_truth_table_vars() const
        { return m_truth_table_vars; }
    bool is_truth_table_set() const { return m_truth_table_set; }
    bool get_truth_table_value() const { return m_truth_table_value; }

    /*
     * Takes a two expressions as argument, the first expression is the
     * condition expression for one case, the second is the expression that
     * should be executed and returned.
     */
    void insert_case(Expression * expr_cond, Expression * expr);
    virtual void print_ast(const std::string & ws) const;
    virtual void accept(AstNodeVisitor & v)  const;
private:
    std::vector<std::pair<Expression *, Expression *>> m_cases;
    std::vector<std::vector<bool> > m_truth_table;
    std::vector<std::string> m_truth_table_vars;
    bool m_truth_table_set = false;
    bool m_truth_table_value;
};

/*
 * A set expressions is a list of values that NuSMV will pick randomly in a init
 * or next assignment.
 */
class SetExpr : public Expression {
public :
    SetExpr() {}
    SetExpr(const SetExpr & se);
    virtual ~SetExpr();
    virtual SetExpr * clone() const { return new SetExpr(*this); }

    std::vector<Expression *> get_expressions() const { return m_expressions; }
    void insert_expr(const Expression * expr);

    virtual void print_ast(const std::string & ws) const;
    virtual void accept(AstNodeVisitor & v)  const;
private:
    std::vector<Expression *> m_expressions;
};

/*
 * A node class that represents an LTL specification. Only the the operators
 * '&', '|' and 'xor' are allowed in LTL expressions except for the outer
 * expression where the operator 'G* can be used.
 *
 * The first constructor takes only one Expression object. This is used for
 * expressions the outer expressions.
 *
 * The second constructor takes two Expression objects. The first is the
 * expression on the left-hand side of the LTL operator, while the second one
 * represents the expression on the right-hand side.
 */
class LTLExpr : public Expression {
public:
    LTLExpr(const Expression * expr, const std::string & symbol = "",
            const std::string & name = "");
    LTLExpr(const Expression * left, const Expression * right,
            const std::string & symbol = "", const std::string & name = "");
    LTLExpr(const LTLExpr & ltl);
    virtual ~LTLExpr();
    virtual LTLExpr * clone() const { return new LTLExpr(*this); }

    std::string get_symbol() const { return m_symbol; }
    std::string get_name() const { return m_name; }
    const Expression * get_left_expression() const { return m_left; }
    const Expression * get_right_expression() const { return m_right; }

    virtual void print_ast(const std::string & ws) const;
    virtual void accept(AstNodeVisitor & v)  const;
private:
    std::string m_symbol;
    std::string m_name;
    Expression * m_left = nullptr;
    Expression * m_right = nullptr;
};

/*
 * Exception class used for throwing exceptions when the AST is built or used
 * wrongly.
 */
class InvalidAstException : public std::runtime_error {
public:
    InvalidAstException(const std::string & message)
        : std::runtime_error("Invalid AST"), m_message(message) {}

    virtual const char* what() const throw() {
        std::ostringstream ss;
        ss << runtime_error::what() << ". " << m_message;
        return ss.str().data();
    }
private:
    std::string m_message;
};

} // namespace nusmvast

#endif // ASTNODE_H
