/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <iostream>

#include "astnode.h"

namespace nusmvast {

LTLExpr::LTLExpr(const Expression * expr, const std::string & symbol,
                 const std::string & name)
{
    m_symbol = symbol;
    m_name = name;
    m_left = expr->clone();
}

LTLExpr::LTLExpr(const Expression * left, const Expression * right,
                 const std::string & symbol, const std::string & name)
{
    m_symbol = symbol;
    m_name = name;
    m_left = left->clone();
    m_right = right->clone();
}

LTLExpr::LTLExpr(const LTLExpr & ltl) {
    m_symbol = ltl.m_symbol;
    m_left = ltl.m_left->clone();

    if (ltl.m_right)
        m_right = ltl.m_right->clone();
}

LTLExpr::~LTLExpr() {
    if (m_left)
        delete m_left;

    if (m_right)
        delete m_right;
}

void LTLExpr::print_ast(const std::string &ws) const {
    using std::cout;
    using std::endl;

    std::string new_ws(ws + "    ");

    cout << "(LTL_EXPR";
    if (m_name.size() > 0)
        cout << " " << m_name;

    cout << ": ";

    if (m_symbol.size() > 0)
        cout << m_symbol << " ";

    cout << "(" << endl;

    cout << new_ws;
    m_left->print_ast(new_ws);
    cout << endl;

    if (m_right) {
        cout << new_ws;
        m_right->print_ast(new_ws);
        cout << endl;
    }

    cout << ws << ")";
}

void LTLExpr::accept(AstNodeVisitor & v) const { v.visit(this); }

} // namespace nusmvast
