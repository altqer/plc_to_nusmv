/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <iostream>

#include "astnode.h"

namespace nusmvast {

using std::string;
using std::vector;

Program::Program(vector<Module *> & module_list) {
    for (auto m : module_list)
        insert_module(m);
}

Program::~Program() {
    auto it = m_module_list.begin();
    for (; it != m_module_list.end(); ++it) {
        delete (*it);
    }

    m_module_list.clear();
}

bool Program::insert_module(Module * m) {
    if (!m)
        return false;

    auto it = symtable.find(m->get_name());
    if (it == symtable.end()) {
        m_module_list.push_back(m);
        symtable.insert(m->get_name());
        return true;
    }

    return false;
}


void Program::print_ast(const string & ws) const {
    using std::cout;
    using std::endl;

    string new_ws("    ");
    cout << ws << "(PROGRAM" << endl;

    for (auto m : m_module_list) {
        m->print_ast(new_ws);
        cout << endl;
    }

    cout << ws << ")";
    cout << endl;
}

void Program::accept(AstNodeVisitor & v) const { v.visit(this); }

} // namespace nusmvast
