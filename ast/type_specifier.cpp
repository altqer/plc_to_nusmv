/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <iostream>

#include "astnode.h"

namespace nusmvast {

TypeSpecifier::TypeSpecifier(std::string type,
                             std::vector<Expression *> & params)
    : m_type(type)
{
    for (Expression * p : params) {
        //Expression * new_param = Expression::initialize_correct_type(p);
        Expression * new_param = p->clone();
        m_params.push_back(new_param);
    }
}

TypeSpecifier::TypeSpecifier(const TypeSpecifier &ts) {
    m_type = ts.m_type;
    for (Expression * p : ts.m_params) {
        //Expression * new_param = Expression::initialize_correct_type(p);
        Expression * new_param = p->clone();
        m_params.push_back(new_param);
    }
}

TypeSpecifier::~TypeSpecifier() {
    auto it = m_params.begin();
    for (; it != m_params.end(); ++it) {
        delete (*it);
    }

    m_params.clear();
}

void TypeSpecifier::insert_param_expr(const Expression * expr) {
    //Expression * new_expr = Expression::initialize_correct_type(expr);
    Expression * new_expr = expr->clone();
    m_params.push_back(new_expr);
}

void TypeSpecifier::print_ast(const std::string &ws) const {
    using std::cout;
    using std::endl;

    std::string new_ws(ws + "    ");

    cout << ws << "(TYPE: " << m_type;
    if (m_params.size() > 0) {
        cout << endl;
        cout << new_ws << "ARGUMENT_LIST:" << endl;
        for (Expression * p : m_params) {
            cout << new_ws;
            p->print_ast(new_ws);
            cout << endl;
        }
        cout << ws;
    }

    cout << ")";
}

void TypeSpecifier::accept(AstNodeVisitor & v) const { v.visit(this); }

} // namespace nusmvast
