/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <iostream>

#include "astnode.h"

namespace nusmvast {

SetExpr::SetExpr(const SetExpr & se) {
    for (Expression * expr : se.m_expressions) {
        Expression * new_expr = expr->clone();
        m_expressions.push_back(new_expr);
    }
}

SetExpr::~SetExpr() {
    auto it = m_expressions.begin();
    for (; it != m_expressions.end(); ++it) {
        delete (*it);
    }

    m_expressions.clear();
}

void SetExpr::insert_expr(const Expression * expr) {
    Expression * new_expr = expr->clone();
    m_expressions.push_back(new_expr);
}

void SetExpr::print_ast(const std::string &ws) const {
    using std::cout;
    using std::endl;

    std::string new_ws(ws + "    ");

    cout << "(SET_EXPR" << endl;

    for (Expression * e : m_expressions) {
        cout << new_ws;
        e->print_ast(new_ws);
        cout << endl;
    }

    cout << ws << ")";
}

void SetExpr::accept(AstNodeVisitor & v) const { v.visit(this); }

} // namespace nusmvast
