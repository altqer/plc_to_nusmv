/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <iostream>

#include "astnode.h"

namespace nusmvast {

VarDecl::VarDecl(const std::string &name, const TypeSpecifier &type)
    : m_name(name)
{
    m_type = new TypeSpecifier(type);
}

VarDecl::VarDecl(const VarDecl &var) : Decl() {
    m_name = var.m_name;
    m_type = new TypeSpecifier(*var.m_type);
}

VarDecl::~VarDecl() {
    delete m_type;
}

std::string VarDecl::get_type() const {
    return m_type->get_type();
}

void VarDecl::print_ast(const std::string & ws) const {
    using std::cout;
    using std::endl;

    std::string new_ws(ws + "    ");

    cout << ws << "(VAR_DECL (NAME " << m_name << ")" << endl;
    m_type->print_ast(new_ws);
    cout << endl;
    cout << ws << ")";
}

void VarDecl::accept(AstNodeVisitor & v) const { v.visit(this); }


} // namespace nusmvast
