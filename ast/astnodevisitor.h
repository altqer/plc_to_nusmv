/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef ASTNODEVISITOR_H
#define ASTNODEVISITOR_H

#include "astnode.h"

namespace nusmvast {

class Program;
class Module;
class Decl;
class ParamDecl;
class VarDecl;
class DefineDecl;
class AssignDecl;
class TypeSpecifier;
class Expression;
class VarExpr;
class LiteralExpr;
class OpExpr;
class CaseExpr;
class SetExpr;
class LTLExpr;


/*
 * The base visistor class used for traversing the NuSMV AST.
 */
class AstNodeVisitor
{
public:
    AstNodeVisitor() {}

    virtual void visit(const Program * node) = 0;
    virtual void visit(const Module * node) = 0;

    virtual void visit(const ParamDecl * node) = 0;
    virtual void visit(const VarDecl * node) = 0;
    virtual void visit(const DefineDecl * node) = 0;
    virtual void visit(const AssignDecl * node) = 0;
    virtual void visit(const TypeSpecifier * node) = 0;

    virtual void visit(const VarExpr * node) = 0;
    virtual void visit(const LiteralExpr * node) = 0;
    virtual void visit(const OpExpr * node) = 0;
    virtual void visit(const CaseExpr * node) = 0;
    virtual void visit(const SetExpr * node) = 0;
    virtual void visit(const LTLExpr * node) = 0;
};

} // namespace nusmvast

#endif // ASTNODEVISITOR_H
