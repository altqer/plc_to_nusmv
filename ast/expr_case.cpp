/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <iostream>

#include "astnode.h"

namespace nusmvast {

CaseExpr::CaseExpr(const std::vector<std::vector<bool> > & truth_table,
                   const std::vector<std::string> & truth_table_vars,
                   bool truth_table_value)
    : m_truth_table(truth_table),
      m_truth_table_vars(truth_table_vars),
      m_truth_table_value(truth_table_value)
{
    m_truth_table_set = true;
}

CaseExpr::CaseExpr(const CaseExpr & expr) {
    m_truth_table_set = expr.m_truth_table_set;

    if (expr.m_truth_table_set) {
        m_truth_table = expr.m_truth_table;
        m_truth_table_vars = expr.m_truth_table_vars;
        m_truth_table_set = expr.m_truth_table_set;
        m_truth_table_value = expr.m_truth_table_value;
    } else {
        for (auto pair : expr.m_cases ) {
            Expression * left = pair.first->clone();
            Expression * right = pair.second->clone();
            m_cases.push_back(std::pair<Expression*, Expression *>(left, right));
        }
    }
}

CaseExpr::~CaseExpr() {
    auto it = m_cases.begin();
    for (; it != m_cases.end(); ++it) {
        delete it->first;
        delete it->second;
    }
    m_cases.clear();

    if (m_truth_table_set) {
        m_truth_table.clear();
        m_truth_table_vars.clear();
        m_truth_table_set = false;
    }
}

void CaseExpr::insert_case(Expression * expr_cond, Expression * expr) {
    if (m_truth_table_set) {
        m_truth_table.clear();
        m_truth_table_vars.clear();
        m_truth_table_set = false;
    }

    Expression * left = expr_cond->clone();
    Expression * right = expr->clone();

    m_cases.push_back(std::pair<Expression*, Expression *>(left, right));
}

void CaseExpr::print_ast(const std::string &ws) const {
    using std::cout;
    using std::endl;

    std::string new_ws(ws + "    ");

    cout << "(CASE_EXPR" << endl;

    for (const std::pair<Expression *, Expression *> & c : m_cases) {
        cout << new_ws;
        c.first->print_ast(new_ws);
        cout << " : ";
        c.second->print_ast(new_ws);
        cout << endl;
    }

    cout << ws << ")";
}

void CaseExpr::accept(AstNodeVisitor & v) const { v.visit(this); }

} // namespace nusmvast
