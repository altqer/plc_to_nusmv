/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <iostream>

#include "astnode.h"

namespace nusmvast {

AssignDecl::AssignDecl(assign_type_enum type, const VarExpr & var,
                       const Expression & expr)
{
    m_assign_type = type;
    m_var = new VarExpr(var);
    //m_expr = Expression::initialize_correct_type(&expr);
    m_expr = expr.clone();
}

AssignDecl::AssignDecl(const AssignDecl & decl) : Decl() {
    m_assign_type = decl.m_assign_type;
    m_var = new VarExpr(*decl.m_var);
    //m_expr = Expression::initialize_correct_type(decl.m_expr);
    m_expr = decl.m_expr->clone();
}

AssignDecl::~AssignDecl() {
    delete m_var;
    delete m_expr;
}

void AssignDecl::print_ast(const std::string & ws) const {
    using std::cout;
    using std::endl;

    std::string new_ws(ws + "    ");

    cout << ws << "(ASSIGN_DECL";

    if (m_assign_type == INIT) {
        cout << " INIT:";
    } else if (m_assign_type == NEXT) {
        cout << " NEXT:";
    }

    cout << endl;
    cout << new_ws;
    m_var->print_ast(new_ws);
    cout << endl;
    cout << new_ws;
    m_expr->print_ast(new_ws);
    cout << endl;

    cout << ws << ")";
}

void AssignDecl::accept(AstNodeVisitor & v) const { v.visit(this); }
} // namespace nusmvast
