/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <iostream>

#include "astnode.h"

namespace nusmvast {

VarExpr::VarExpr(std::string identifier, const VarExpr & expr)
    : m_identifier(identifier)
{
    m_expr = new VarExpr(expr);
    m_expr_set = true;
}

VarExpr::VarExpr(const VarExpr & expr) {
    m_identifier = expr.m_identifier;
    if (expr.m_expr_set) {
        m_expr = new VarExpr(*(expr.m_expr));
        m_expr_set = true;
    }
}

VarExpr::~VarExpr() {
    if (m_expr_set) {
        delete m_expr;
    }
}

void VarExpr::print_ast(const std::string & ws) const {
    using std::cout;
    using std::endl;

    cout << "(";

    if (m_expr_set) {

        cout << "(NAME " << m_identifier << ") . ";
        m_expr->print_ast(ws);
    } else {
        cout << "NAME " << m_identifier;
    }

    cout << ")";
}

void VarExpr::accept(AstNodeVisitor & v) const { v.visit(this); }

} // namespace nusmvast
