/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <iostream>

#include "astnode.h"

namespace nusmvast {

using std::string;
using std::vector;

Module::Module(const string & name, const std::vector<ParamDecl> & params) {
    m_name = name;

    for (const ParamDecl & pd : params)
        insert_param(pd);
}

TypeSpecifier Module::get_type_specifier() const {
    TypeSpecifier ts(m_name);

    for (const ParamDecl & d : m_params) {
        VarExpr var(d.get_name());
        ts.insert_param_expr(&var);
    }

    return ts;
}

bool Module::insert_param(const ParamDecl & pd) {
    auto it = symtable.find(pd.get_name());
    if (it == symtable.end()) {
        m_params.push_back(pd);
        symtable.insert(pd.get_name());
        return true;
    }

    return false;
}

bool Module::insert_var(const VarDecl & vd) {
    auto it = symtable.find(vd.get_name());
    if (it == symtable.end()) {
        m_vars.push_back(vd);
        symtable.insert(vd.get_name());
        return true;
    }

    return false;
}

bool Module::insert_define(const DefineDecl & dd) {
    auto it = symtable.find(dd.get_identifier());
    if (it == symtable.end()) {
        m_defines.push_back(dd);
        symtable.insert(dd.get_identifier());
        return true;
    }

    return false;
}

bool Module::insert_assign(const AssignDecl & ad) {
    m_assign_constraints.push_back(ad);
    return true;
}

bool Module::insert_ltl(const LTLExpr &ltl) {
    m_ltl_specs.push_back(ltl);
    return true;
}

void Module::print_ast(const string & ws) const {
    using std::cout;
    using std::endl;

    string new_ws(ws + "    ");

    cout << ws << "(MODULE (NAME " << m_name << ")" << endl;

    for (const ParamDecl & d : m_params) {
        d.print_ast(new_ws);
        cout << endl;
    }

    for (const VarDecl & d : m_vars) {
        d.print_ast(new_ws);
        cout << endl;
    }

    for (const DefineDecl & d : m_defines) {
        d.print_ast(new_ws);;
        cout << endl;
    }

    for (const AssignDecl & ac : m_assign_constraints) {
        ac.print_ast(new_ws);
        cout << endl;
    }

    for (const LTLExpr & ltl : m_ltl_specs) {
        cout << new_ws;
        ltl.print_ast(new_ws);
        cout << endl;
    }

    cout << ws << ")";
}

void Module::accept(AstNodeVisitor & v) const { v.visit(this); }

} // namespace nusmvast
