/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <sstream>
#include <iomanip>
#include <cmath>
#include <climits>

#include "testcasegenerator.h"

namespace plctonusmv {

POU TestCaseGenerator::generate_fbd() const {
    POU std_pou_and = POU::get_standard_pou("AND");
    POU std_pou_or = POU::get_standard_pou("OR");
    size_t nr_of_or_gates = ceil(m_operators / 2.0);
    size_t nr_of_and_gates = floor(m_operators / 2.0);

    vector<string> in_var_names = generate_var_names("in", m_inputs);
    vector<string> out_var_names = generate_var_names("out", m_outputs);
    vector<string> or_gate_names = generate_var_names("or_gate",
                                                      nr_of_or_gates);
    vector<string> and_gate_names = generate_var_names("and_gate",
                                                      nr_of_and_gates);
    vector<PlcVariable> in_vars;
    vector<PlcVariable> out_vars;
    vector<PlcVariable> in_out_vars;

    for (const string & var_name : in_var_names)
        in_vars.push_back( {var_name, "input"} );

    for (const string & var_name : out_var_names)
        out_vars.push_back( {var_name, "output"} );

    std::map<int, Node> nodes;
    std::map<string, int> var_node_map;
    std::unordered_map<int, int> connected_count;

    int local_id = 1;
    for (const string & var_name : in_var_names) {
        nodes.insert( {local_id, Node(local_id, VARIABLE,
                       PlcVariable(var_name, "input"))} );
        var_node_map.insert( {var_name, local_id} );
        connected_count.insert( {local_id, 0});
        local_id++;
    }

    bool or_gate = true;
    size_t or_index = 0;
    size_t and_index = 0;
    size_t gates_index = 0;

    while ((or_gate && or_index < nr_of_or_gates) ||
           (!or_gate && and_index < nr_of_and_gates))
    {
        Node * new_node;
        if (or_gate) {
            new_node = new Node(local_id, POU_INSTANCE, std_pou_or,
                              or_gate_names.at(or_index));
            or_index++;
        } else {
            new_node = new Node(local_id, POU_INSTANCE, std_pou_and,
                              and_gate_names.at(and_index));
            and_index++;
        }

        int id_in1 = get_first_node(nodes, connected_count);

        connected_count[id_in1]++;
        int id_in2 = get_first_node(nodes, connected_count);

        connected_count[id_in2]++;
        if (id_in1 < id_in2) {
            new_node->insert_edge( {id_in1, "IN1"} );
            new_node->insert_edge( {id_in2, "IN2"} );
        } else {
            new_node->insert_edge( {id_in2, "IN1"} );
            new_node->insert_edge( {id_in1, "IN2"} );
        }

        nodes.insert( {local_id, *new_node} );
        var_node_map.insert( {new_node->get_node_name(), local_id} );
        connected_count.insert( {local_id, 0});

        delete new_node;
        local_id++;
        gates_index++;
        or_gate = !or_gate;
    } // end of while


    for (const string & var_name : out_var_names) {
        int id_in = get_first_node(nodes, connected_count, false);
        Node new_node(local_id, VARIABLE, PlcVariable(var_name, "output"));
        new_node.insert_edge( {id_in} );
        nodes.insert( {local_id, new_node} );
        var_node_map.insert( {var_name, local_id});

        connected_count[id_in] += 2;
        local_id++;
    }

    POU pou("FBDTest", in_vars, out_vars, in_out_vars, FBDGraph(nodes));
    pou.insert_var_node_map(var_node_map);
    return pou;
} // end of method generate_fbd()

vector<string> TestCaseGenerator::generate_var_names(const std::string & prefix,
                                                     size_t vars)
{
    vector<string> var_names;

    size_t leading_zeros = 0;
    size_t tmp = vars - 1;
    while (tmp >= 10) {
        leading_zeros++;
        tmp = tmp / 10;
    }

    std::stringstream ss;

    if (leading_zeros++ > 0)
        ss << std::setfill('0');


    for (size_t i = 0; i < vars; i++) {
        ss << prefix << std::setw(leading_zeros) << i;
        var_names.push_back(ss.str());
        ss.str("");
    }

    return var_names;
}

int TestCaseGenerator::get_first_node(
        const std::map<int, Node> &nodes,
        const std::unordered_map<int, int> & connected_count,
        bool from_top)
{

    int prev_id = 0;
    int prev_count = INT_MAX;

    if (from_top) {
        for (const std::pair<int, Node> & pair : nodes) {
            const Node & node = pair.second;
            if (node.get_node_type() == VARIABLE &&
                    node.get_variable()->get_type() == "output")
            {
                continue;
            }
            int local_id = node.get_local_id();
            int count = connected_count.find(local_id)->second;
            if (count < prev_count) {
                prev_count = count;
                prev_id = local_id;
            }
        }
    } else {
        for (auto it = nodes.rbegin(); it != nodes.rend(); ++it) {
            const Node & node = it->second;
            if (node.get_node_type() == VARIABLE &&
                    node.get_variable()->get_type() == "output")
            {
                continue;
            }

            int local_id = node.get_local_id();
            int count = connected_count.find(local_id)->second;
            if (count < prev_count) {
                prev_count = count;
                prev_id = local_id;
            }
        }
    }

    return prev_id;
}

} // namespace plctonusm
