/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef PLCREADER_H
#define PLCREADER_H

#include <QXmlStreamReader>

#include <string>
#include <vector>

#include "pou.h"

namespace plctonusmv {

/*
 * Also known as the XML parser. It validates and parses the XML file given
 * as input to the constructor.
 */
class PlcReader
{
public:
    PlcReader(const std::string & xml_file,
              const std::string & schema_file = "");

    /*
     * Parses the XMl file and extracts the root POU object that represents
     * the entire PLC program.
     */
    POU generate_fbd();

    /*
     * Returns true if non-critical errors were found during the parsing of
     * the XML file.
     */
    bool has_errors() const { return m_errors.size() > 0; }

    /*
     * Returns the list of non-critical errors found during the parsing of
     * the XML file.
     */
    const std::vector<std::string> & get_errors() const { return m_errors; }
private:
    std::string m_xml_file;
    std::string m_schema_file;
    std::vector<std::string> m_errors;

    /*
     * Checks if the XML file is valid and confirms to the PLCopen XML schema.
     */
    bool validate_xml();
    void read_plc_from_xml(POU & pou);
    std::map<string, vector<PlcVariable>> read_all_variables(
            QXmlStreamReader & xml) const;
    vector<PlcVariable> read_variables(QXmlStreamReader & xml,
                                       const string & type) const;
    void read_pou_body(QXmlStreamReader & xml, POU & pou);
    void read_fbd(QXmlStreamReader & xml, POU & pou);
    Node read_fbd_in_variable(QXmlStreamReader & xml) const;
    Node read_fbd_out_variable(QXmlStreamReader & xml) const;
    Node read_fbd_block(QXmlStreamReader & xml) const;
    int read_connection_point_in(QXmlStreamReader & xml) const;

    static string generate_var_name(const string & prefix, size_t vars,
                                    size_t number);
};

} // namespace plctonusm

#endif // PLCREADER_H
