/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef ASTBUILDER_H
#define ASTBUILDER_H

#include "pou.h"
#include "ast/astnode.h"
#include "truthtablereader.h"

namespace plctonusmv {

/*
 * A class that is used to build a NuSMV AST.
 */
class ASTBuilder
{
public:
    ASTBuilder(const POU * pou, bool optimize, const TruthTable * tt = nullptr)
        { m_pou = pou; m_tt = tt; m_optimize = optimize; }

    /*
     * Builds a NuSMV AST and returns the root node (the Program object) of the
     * AST.
     */
    nusmvast::Program * build_ast() const;
private:
    // The PLC program in the POU data structure
    const POU * m_pou;
    bool m_optimize;

    // The truth table object. It is set if a truth table file is given as an
    // argument to the PLC-NuSMV compiler.
    const TruthTable * m_tt;

    /*
     * Creates a module object that represents the PLC program. It takes a list
     * of parameters and variable declarations as parameters.
     */
    nusmvast::Module * create_fbd_module(
        const vector<nusmvast::ParamDecl> & params,
        const vector<nusmvast::VarDecl> & vars
    ) const;

    /*
     * Creates a module object that represents the truth table. It takes a list
     * of parameters, list of variable declarations and a data structure that
     * represents the actual truth table.
     */
    nusmvast::Module * create_truth_table_module(
        const vector<nusmvast::ParamDecl> & params,
        const vector<nusmvast::VarDecl> & vars,
        const std::unordered_map< string, std::pair<bool,
            vector<vector<bool>>> > & truth_table
    ) const;

    /*
     * Creates the main module of the NuSMV model. It needs a reference to the
     * other two modules as parameters so that the main module can instantiate
     * the two modules.
     */
    nusmvast::Module * create_main_module(
        const nusmvast::Module * fbd_module,
        const nusmvast::Module * truth_table_module
    ) const;

    /*
     * Reads the TruthTable object and constructs a truth table in a map data
     * structure where the key is the output variable and the value is a Pair
     * object. The first value in the Pair is the Boolean value each row in the
     * truth table in the second value of the pair object results in. The second
     * value in the pair is a two-dimensional vector of Boolean values. Each
     * cell in this table represents the Boolean value of an input variable. On
     * each row of the table you can see what combination of values of the input
     * variable results in the Boolean value in the first value of the Pair
     * object.
     *
     * The reason for creating such a data structure is to avoid having the
     * entire truth table in a case expression in the NuSMV model. The smallest
     * of the truth table for either the Boolean values True or False is chosen.
     * In addition to reducing the size of the NuSMV model, it also reduces the
     * compilation time.
     */
    std::unordered_map<std::string, std::pair<bool, vector<vector<bool>>> >
        build_truth_table() const;

    /*
     * Constructs a truth table in the data structure explained in the
     * build_truth_table() method. The difference from the other method is that
     * the truth table here is generated by traversing the AST. This is mostly
     * used for PLC programs that have been generated automatically by the
     * compiler using the Test Case Generator.
     */
    std::unordered_map<std::string, std::pair<bool, vector<vector<bool>>> >
        build_truth_table_from_ast() const;

    /*
     * Takes an output variable object as parameter and builds the Boolean
     * formula for that output variable. The formula is represented by an AST of
     * OpExpr and VarExpr nodes.
     *
     * The formula build_formula_ast_rec() is called to start the actual
     * construction of the formula AST.
     */
    nusmvast::Expression * build_formula_ast(const PlcVariable & var) const;

    /*
     * Builds the AST by traversing the graph from the node given as argument.
     * Returns the root node (Expression object) of the expression for the Node
     * object given as argument.
     */
    nusmvast::Expression * build_formula_ast_rec(const Node * node) const;

    /*
     * Returns the result of a Boolean formula given the a list of input
     * variables that are true. The AST of the formula is traversed and
     */
    bool check_formula(
            const nusmvast::Expression * expr,
            const std::unordered_set<string> & true_input_vars
    ) const;

    /*
     * Creates a DefineDecl AST node that represents a declaration in the
     * DEFINE section of a NuSMV module. The node given as argument must have
     * incoming edges.
     */
    const nusmvast::DefineDecl create_define_decl(const Node & node) const;

    /*
     * Returns the NuSMV equivalent symbol for the POU data structure Boolean
     * symbol. For example, the POU type AND yields the '&' symbol.
     */
    const string get_symbol(const POU & pou) const;

    /*
     * Builds and returns a case expression for an output variable in the
     * TruthTable Module. The case expression should be placed in a init
     * assignment in the ASSIGN section of a Module.
     */
    nusmvast::CaseExpr build_truth_table_case_expr(
        bool tt_true,
        const vector<vector<bool>> & truth_table,
        const vector<PlcVariable> & input_vars
    ) const;

    /*
     * Builds an a LTL AST node that represents an LTL expression. The list of
     * output variables and the name of the Module instances must be given as
     * argument to this function.
     */
    const nusmvast::LTLExpr create_ltl_expr(
        const string & fbd_name,
        const string & truth_table_name,
        const vector<PlcVariable> & output_vars
    ) const;
};

} // namespace plctonusm

#endif // ASTBUILDER_H
