/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef PLCTONUSM_H
#define PLCTONUSM_H

#include <string>
#include <unordered_map>
#include <map>
#include <fstream>

#include "node.h"
#include "pou.h"
#include "edge.h"
#include "ast/astnode.h"
#include "astbuilder.h"
#include "testcasegenerator.h"
#include "truthtablereader.h"
#include "plctonusmvlogger.h"

using std::string;

namespace plctonusmv {

/*
 * The class that contains the functions to start the most importan jobs of
 * the compiler. It is used for compile PLC programs to NuSMV and generating
 * test cases.
 *
 * The path of the PLC program (in the form of a XML file), the truth table
 * file, the NuSMV file and the XML shema file. The last option is ignored.
 */
class PlcToNusmv {
public:
    PlcToNusmv(const string & plc_xml_file, const string & truthtable_file,
               const string & nusmv_file, const string & xml_schema,
               bool optimize,
               PlcToNusmvLogger & logger)
        : m_plc_xml_file(plc_xml_file),
          m_truthtable_file(truthtable_file),
          m_nusmv_file(nusmv_file),
          m_xml_schema(xml_schema),
          m_optimize(optimize),
          m_logger(logger)
    {}

    void show_file_names() const; // Debugging

    /*
     * Starts the parsing of of the PLC program and then starts the compilation
     * process.
     */
    void process_input_files();

    /*
     * Compiles the PLC program to NuSMV.
     */
    void compile_to_nusm(const POU * main_pou, const TruthTable * tt = nullptr);

    /*
     * Generates test cases with a variable number of input and output
     * variables.
     */
    void generate_test_case(size_t inputs, size_t outputs);
private:
    string m_plc_xml_file;
    string m_truthtable_file;
    string m_nusmv_file;
    string m_xml_schema;
    bool m_optimize;
    PlcToNusmvLogger & m_logger;

    // Private methods
    POU build_main_pou();
    TruthTable build_truth_table();
    void create_nusmv_file(const nusmvast::Program * program) const;
};

} // namespace plctonusm

#endif // PLCTONUSM_H
