/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef TRUTHTABLEREADER_H
#define TRUTHTABLEREADER_H

#include <string>
#include <vector>
#include <regex>
#include <map>

namespace plctonusmv {


/*
 * A struct that represents a truth table.
 *
 * - vars is the list of variable names
 * - tt is the 2D-array that represents the actual truth table
 * - A data structure that maps the name of a variable to the column index
 *   of the truth table (tt) that the variable is supposed to be in.
 */
struct TruthTable {
    std::vector<std::string> vars;
    std::vector<std::vector<bool>> tt;
    std::map<std::string, size_t> var_map;
};

/*
 * Used for reading truth table files. The path of the truth table file must
 * be given as argument.
 */
class TruthTableReader
{
public:
    TruthTableReader(const std::string & truthtable_file)
        : m_truthtable_file(truthtable_file) {}
    /*
     * Reads the truth table file and returns a TruthTable struct which
     * represents the truth table.
     */
    TruthTable generate_truth_table() const;
private:
    std::string m_truthtable_file;
    static const std::regex Re_vars;
    static const std::regex Re_tt;
};


} // namespace plctonusmv

#endif // TRUTHTABLEREADER_H
