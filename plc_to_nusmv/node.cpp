/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include "node.h"

namespace plctonusmv {

Node::Node(int local_id, node_type type, const PlcVariable & var,
           const string & node_name)
    : m_local_id(local_id), m_node_type(type)
{
    m_var = new PlcVariable(var);
    if (node_name.size() > 0)
        m_node_name = node_name;
    else
        m_node_name = m_var->get_name();
}

Node::Node(int local_id, node_type type, const POU & pou,
           const string & node_name)
    : m_local_id(local_id), m_node_type(type)
{
    m_pou = new POU(pou);
    m_node_name = node_name;
}

Node::Node(const Node & n) {
    m_local_id = n.m_local_id;
    m_node_name = n.m_node_name;
    m_node_type = n.m_node_type;
    m_in_edges = n.m_in_edges;

    if (n.m_node_type == VARIABLE)
        m_var = new PlcVariable(*n.m_var);
    else
        m_pou = new POU(*n.m_pou);
}

Node & Node::operator=(const Node & n) {
    if (this != &n) {
        m_local_id = n.m_local_id;
        m_node_name = n.m_node_name;
        m_node_type = n.m_node_type;
        m_in_edges = n.m_in_edges;

        if (m_node_type == VARIABLE)
            delete m_var;
        else
            delete m_pou;

        if (n.m_node_type == VARIABLE)
            m_var = new PlcVariable(*n.m_var);
        else
            m_pou = new POU(*n.m_pou);
    }

    return *this;
}

Node::~Node() {
    if (m_node_type == VARIABLE)
        delete m_var;
    else
        delete m_pou;
}

void Node::insert_edge(Edge e) {
    m_in_edges.push_back(e);
}

void Node::show_info() const {
    using std::cout;
    using std::endl;

    if (m_node_type == VARIABLE) {
        cout << "Variable (name: " << m_var->get_name()
             << ", id: " << m_local_id
             << ", type: " << m_var->get_type()
             << ", plc data type: " << m_var->get_plc_type();
        if (m_var->get_type() == "output") {
            cout << ", in-edges: {";
            show_edges();
            cout << "}";
        }
        cout << endl;
    } else {
        if (m_pou->is_standard_pou()) {
            cout << "Standard POU function";
        } else {
            cout << "POU instance";
        }

        cout << " (name: "  << m_node_name
             << ", type: "  << m_pou->get_type()
             << ", id: "    << m_local_id
             << ", in-edges: {";
        show_edges();
        cout << "}" << endl;
    }
}

void Node::show_edges() const {
    using std::cout;
    using std::endl;

    for (Edge e : m_in_edges) {
        cout << " <ref-id: " << e.get_ref_local_id()
             << ", param-name: " << e.get_parameter_name()
             << "> ";
    }
}

} // namespace plctonusmv
