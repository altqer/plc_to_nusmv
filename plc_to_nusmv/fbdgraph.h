/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef FBDGRAPH_H
#define FBDGRAPH_H

#include <map>

#include "node.h"

namespace plctonusmv {

class Node;

/*
 * The class that represents the graph in a PLC program.
 */
class FBDGraph
{
private:
    std::map<int, Node> m_nodes;
public:
    FBDGraph() {}
    FBDGraph(std::map<int, Node> nodes) : m_nodes(nodes) {}

    /*
     * Get a ref to list of all the node in a map data structure where the key
     * is the local id of the node and Node, the actual Node object.
     */
    const std::map<int, Node> & get_nodes() const { return m_nodes; }

    /*
     * Tries to find the Node in the graph with the given local ID and returns
     * a point to that Node. If the node doesn't exist, a nullptr will be
     * returned.
     */
    const Node * get_node(int local_id) const;
    void show_info() const;
};

} // namespace plctonusm

#endif // FBDGRAPH_H
