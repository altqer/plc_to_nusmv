/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef EDGE_H
#define EDGE_H

#include <string>

namespace plctonusmv {


/*
 * The class that represents an Edge in a graph. The graph can be attached
 * to a node if there is an incoming edge to that node.
 */
class Edge
{
private:
    int m_ref_local_id;
    std::string m_parameter_name;
public:
    Edge(int ref_local_id, const std::string & paramenter_name = "");

    /*
     * Returns the local id of the node the edge is connected to at the tail
     * end of the edge.
     */
    int get_ref_local_id() const { return m_ref_local_id; }

    /*
     * Get the name of the variable of the node this is edge is connected to
     * at the tail end of the edge.
     */
    std::string get_parameter_name() const { return m_parameter_name; }
};

} // namespace plctonusm

#endif // EDGE_H
