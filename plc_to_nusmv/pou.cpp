/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <iostream>

#include "pou.h"
#include "excepts.h"

namespace plctonusmv {

const std::unordered_map<string, POU> POU::standard_pous = create_std_pou_map();

POU::POU() {
    POU("Standard", false);
    m_graph = new FBDGraph();
}

POU::POU(const std::string & type, bool standard_pou) : m_type(type),
    m_standard_pou(standard_pou)
{
    m_graph = new FBDGraph();
}

POU::POU(const string & type,
         const vector<PlcVariable> & in_vars,
         const vector<PlcVariable> & out_vars,
         const vector<PlcVariable> & in_out_vars,
         const FBDGraph & graph,
         bool standard_pou) {
    m_type = type;
    input_variables = in_vars;
    output_variables = out_vars;
    input_output_variables = in_out_vars;
    m_standard_pou = standard_pou;
    m_graph = new FBDGraph(graph);
}

POU::POU(const POU &pou) {
    m_type = pou.m_type;
    input_variables = pou.input_variables;
    output_variables = pou.output_variables;
    input_output_variables = pou.input_output_variables;
    m_var_node_map = pou.m_var_node_map;
    m_standard_pou = pou.m_standard_pou;
    m_graph = new FBDGraph(*pou.m_graph);

}

POU & POU::operator=(const POU & pou) {
    if (this != &pou) {
        m_type = pou.m_type;
        input_variables = pou.input_variables;
        output_variables = pou.output_variables;
        input_output_variables = pou.input_output_variables;
        m_var_node_map = pou.m_var_node_map;
        m_standard_pou = pou.m_standard_pou;

        delete m_graph;
        m_graph = new FBDGraph(*pou.m_graph);

    }

    return *this;
}

POU::~POU() {
    delete m_graph;
}

void POU::insert_variables(const std::string & type,
                           const vector<PlcVariable> & vars)
{
    void (POU::*insert_var)(const PlcVariable &);
    if (type == "input")
        insert_var = &POU::insert_input_variable;
    else if (type == "output")
        insert_var = &POU::insert_output_variable;
    else if (type == "in_out")
        insert_var = &POU::insert_input_output_variable;
    else
        return;

    for (const PlcVariable & var : vars)
        (this->*insert_var)(var);
}

void POU::insert_input_variable(const PlcVariable & variable) {
    input_variables.push_back(variable);
}

void POU::insert_output_variable(const PlcVariable & variable) {
    output_variables.push_back(variable);
}

void POU::insert_input_output_variable(const PlcVariable & variable) {
    input_output_variables.push_back(variable);
}

void POU::insert_var_node_map(const std::map<std::string, int> & var_node_map) {
    m_var_node_map = var_node_map;
}

void POU::show_variables() const {
    using std::cout;
    using std::endl;

    cout << "Input variables: " << endl;
    for(const PlcVariable & in_var : input_variables) {
        cout << in_var.get_name() << endl;
    }

    cout << "Output variables: " << endl;
    for(const PlcVariable & out_var : output_variables) {
        cout << out_var.get_name() << endl;
    }

    if (input_output_variables.size() > 0) {
        cout << "Input-output variables: " << endl;
        for(const PlcVariable & out_in_var : input_output_variables) {
            cout << out_in_var.get_name() << endl;
        }
    }
}

void POU::show_graph_info() const {
    m_graph->show_info();
}

void POU::insert_graph(const FBDGraph & graph) {
    delete m_graph;
    m_graph = new FBDGraph(graph);
}

POU POU::get_standard_pou(const std::string &pou_name) {
    auto it = standard_pous.find(pou_name);
    if (it == standard_pous.end())
        throw InvalidStandardPouException(pou_name);

    return it->second;
}

bool POU::is_std_pou_type(const std::string &pou_type) {
    auto it = standard_pous.find(pou_type);
    return it != standard_pous.end();
}

} // namespace plctonusmv
