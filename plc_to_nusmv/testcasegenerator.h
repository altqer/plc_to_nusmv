/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef TESTCASEGENERATOR_H
#define TESTCASEGENERATOR_H

#include "pou.h"

namespace plctonusmv {

/*
 * Generates test cases where the complexity of the test cases can be
 * set witht the number of input of input and output variables.
 */
class TestCaseGenerator
{
public:
    TestCaseGenerator(size_t inputs, size_t outputs)
        : m_inputs(inputs), m_outputs(outputs), m_operators(inputs - 1) {}

    /*
     * Generates the test case and return the POU object that represents the
     * PLC program.
     */
    POU generate_fbd() const;
private:
    size_t m_inputs;
    size_t m_outputs;
    size_t m_operators;

    static vector<string> generate_var_names(const string & prefix,
                                             size_t vars);
    static int get_first_node(
            const std::map<int, Node> & nodes,
            const std::unordered_map<int, int> & connected_count,
            bool from_top = true
    );
};

} // namespace plctonusm

#endif // TESTCASEGENERATOR_H
