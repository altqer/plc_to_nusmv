/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <iostream>
#include <fstream>

#include "truthtablereader.h"
#include "excepts.h"


namespace plctonusmv {

const std::regex TruthTableReader::Re_vars(R"(([a-z]\w*))");
const std::regex TruthTableReader::Re_tt(R"((0|1))");

TruthTable TruthTableReader::generate_truth_table() const {
    TruthTable truth_table;

    using std::string;
    using std::regex;

    std::ifstream ttin;
    ttin.open(m_truthtable_file);

    if (!ttin.is_open()) {
        throw TruthTableException("Can't open the truth table file");
    }

    string line;

    // Reading the variable names
    size_t var_index = 0;
    if (getline(ttin, line)) {
        std::sregex_token_iterator re_it_vars(line.begin(), line.end(),
                                              Re_vars, 0);
        std::sregex_token_iterator re_it_end;

        while (re_it_vars != re_it_end) {
            truth_table.vars.push_back(*re_it_vars);
            truth_table.var_map.insert( {*re_it_vars, var_index++} );
            re_it_vars++;
        }
    }

    if (var_index < 2) {
        throw TruthTableException("Too few variable names (must be at least 2)",
                                  1);
    }

    // Reading the truth table values
    long line_nr = 1;
    while (getline(ttin, line)) {
        line_nr++;
        std::sregex_token_iterator re_it_vars(line.begin(), line.end(),
                                              Re_tt, 0);
        std::sregex_token_iterator re_it_end;

        std::vector<bool> tt_row;

        while (re_it_vars != re_it_end) {
            if (*re_it_vars == "1")
                tt_row.push_back(true);
            else
                tt_row.push_back(false);

            re_it_vars++;
        }

        if (truth_table.vars.size() != tt_row.size()) {
            string msg = "The number of variables in the first line ";
            msg += " doesn't match the number of boolean values";
            throw TruthTableException(msg, line_nr);
        }

        truth_table.tt.push_back(tt_row);
    }

    ttin.close();

    return truth_table;
}

} // namespace plctonusmv
