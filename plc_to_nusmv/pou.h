/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef POU_H
#define POU_H

#include <string>
#include <vector>
#include <unordered_map>
#include <map>

#include "plcvariable.h"
#include "node.h"
#include "fbdgraph.h"

using std::string;
using std::vector;

namespace plctonusmv {

class FBDGraph;

/*
 * Represnts a POU type. It is attached to a Node object and then placed in
 * a graph.
 */
class POU {
public:
    enum pou_std_type {AND, OR, XOR};

    POU();
    POU(const string & type, bool standard_pou = false);
    POU(const string & type,
        const vector<PlcVariable> & in_vars,
        const vector<PlcVariable> & out_vars,
        const vector<PlcVariable> & in_out_vars,
        const FBDGraph & graph,
        bool standard_pou = false
    );
    POU(const POU & pou);
    POU & operator=(const POU & pou);
    ~POU();

    string get_type() const { return m_type; }
    void set_type(const string & type) { m_type = type; }
    bool is_standard_pou() const { return m_standard_pou; }
    const vector<PlcVariable> & get_input_variables() const
        { return input_variables; }
    const vector<PlcVariable> & get_output_variables() const
        { return output_variables; }
    const vector<PlcVariable> & get_input_output_variables() const
        { return input_output_variables; }
    const std::map<string, int> & get_var_node_map() const
        { return m_var_node_map; }

    void insert_variables(const string & type,
                          const vector<PlcVariable> & vars);

    void insert_input_variable(const PlcVariable & variable);
    void insert_input_variable(const string & var_name)
        { insert_input_variable(PlcVariable(var_name, "input", "boolean")); }
    void insert_output_variable(const PlcVariable & variable);
    void insert_output_variable(const string & var_name)
        { insert_output_variable(PlcVariable(var_name, "output", "boolean")); }
    void insert_input_output_variable(const PlcVariable & variable);

    void insert_var_node_map(const std::map<string, int> & var_node_map);

    void insert_graph(const FBDGraph & graph);
    const FBDGraph & get_graph() const { return *m_graph; }

    // Debugging
    void show_variables() const;
    void show_graph_info() const;

    // Static initializations
    static POU get_standard_pou(const string & pou_name);
    static bool is_std_pou_type(const string & pou_type);
private:
    string m_type;
    bool m_standard_pou;

    // Variables
    vector<PlcVariable> input_variables;
    vector<PlcVariable> output_variables;
    vector<PlcVariable> input_output_variables;
    std::map<string, int> m_var_node_map;

    FBDGraph * m_graph;

    /*static std::unordered_map<string, int> standard_pous = {
        {"AND", 2},
    };*/

    static std::unordered_map<string, POU> create_std_pou_map() {
        std::unordered_map<string, POU> map = {
            {"AND", {"AND", true}},
            {"OR", {"OR", true}},
            {"XOR", {"XOR", true}},
            {"NOT", {"NOT", true}},
        };

        map.find("AND")->second.insert_input_variable("IN1");
        map.find("AND")->second.insert_input_variable("IN2");
        map.find("AND")->second.insert_output_variable("OUT");

        map.find("OR")->second.insert_input_variable("IN1");
        map.find("OR")->second.insert_input_variable("IN2");
        map.find("OR")->second.insert_output_variable("OUT");

        map.find("XOR")->second.insert_input_variable("IN1");
        map.find("XOR")->second.insert_input_variable("IN2");
        map.find("XOR")->second.insert_output_variable("OUT");

        map.find("NOT")->second.insert_input_variable("IN");
        map.find("NOT")->second.insert_output_variable("OUT");

        return map;
    }

    static const std::unordered_map<string, POU> standard_pous;
};

} // namespace plctonusm

#endif // POU_H
