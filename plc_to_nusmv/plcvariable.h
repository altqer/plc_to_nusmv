/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef PLCVARIABLE_H
#define PLCVARIABLE_H

#include <string>

using std::string;

namespace plctonusmv {

/*
 * A variable that is attached to a Node class and then placed in a FBD graph.
 */
class PlcVariable
{
private:
    string m_name;
    string m_type;
    string m_plc_type;
public:
    PlcVariable();
    PlcVariable(const string & name, const string & type,
                const string & plc_type = "boolean");
    PlcVariable(const PlcVariable & var);

    string get_name() const { return m_name; }
    string get_type() const { return m_type; }
    string get_plc_type() const { return m_plc_type; }
};

} // namespace plctonusm

#endif // PLCVARIABLE_H
