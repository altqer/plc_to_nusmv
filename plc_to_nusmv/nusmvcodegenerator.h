/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef NUSMVCODEGENERATOR_H
#define NUSMVCODEGENERATOR_H

#include <sstream>

#include "ast/astnode.h"
#include "ast/astnodevisitor.h"

using namespace nusmvast;

namespace plctonusmv {

/*
 * The visitor class that traveres NuSMV ASTs and generates NuSMV code.
 */
class NusmvCodeGenerator : public AstNodeVisitor
{
public:
    NusmvCodeGenerator(std::ostream & output_stream,
                       const std::string original_indent = "")
        : os(output_stream), indent(original_indent) {}

    /*
     * Methods that generate NuSMV code for each type of NuSMV AST node.
     */
    virtual void visit(const Program * node);
    virtual void visit(const Module * node);

    // Declerations
    virtual void visit(const ParamDecl * node);
    virtual void visit(const VarDecl * node);
    virtual void visit(const DefineDecl * node);
    virtual void visit(const AssignDecl * node);

    virtual void visit(const TypeSpecifier * node);

    // Expressions
    virtual void visit(const VarExpr * node);
    virtual void visit(const LiteralExpr * node);
    virtual void visit(const OpExpr * node);
    virtual void visit(const CaseExpr * node);
    virtual void visit(const SetExpr * node);
    virtual void visit(const LTLExpr * node);
private:
    std::ostream & os;
    std::string indent;

    // Used for checking if a symbol is a valid symbol in LTL specifications
    static bool is_ltl_symbol(const std::string & symbol);
};

} // namespace plctonusm

#endif // NUSMVCODEGENERATOR_H
