/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef EXCEPTS_H
#define EXCEPTS_H

#include <exception>
#include <stdexcept>
#include <sstream>

namespace plctonusmv {

/*
 * Contains Exception classes used by many of the plc_to_nusmv classes.
 */

class PlcToNusmvException {
public:
    PlcToNusmvException(const std::string & message) : m_message(message) {}

    virtual std::string what() const {
        return m_message;
    }
private:
    std::string m_message;
};

class PlcXmlException : public PlcToNusmvException {
public:
    PlcXmlException() : PlcToNusmvException("Invalid PLC XML file") {}

    virtual std::string what() const = 0;
};

class PlcXmlSchemaValidatorException : public PlcXmlException {
public:
    PlcXmlSchemaValidatorException(const std::string & filename)
        : m_filename(filename) {}

    virtual std::string what() const {
        std::ostringstream msg;
        msg << PlcToNusmvException::what() << ". "
            << "PLC XML file doesn't conform to the PLCopen XML schema: "
            << m_filename;
        return msg.str();
    }
private:
    std::string m_filename;
};

class InvalidPlcXmlException : public PlcXmlException {
public:
    InvalidPlcXmlException(const std::string & message,
                           long line, long col = -1)
        : m_msg(message), m_line(line), m_col(col) {}

    virtual std::string what() const {
        std::ostringstream msg;
        msg << PlcToNusmvException::what() << ". "
            << m_msg << " at line " << m_line;
        if (m_col >= 0)
            msg << ", column " << m_col;
        return msg.str().c_str();
    }

private:
    std::string m_msg;
    long m_line;
    long m_col;
};

class TruthTableException : public PlcToNusmvException {
public:
    TruthTableException(const std::string & message, long line = -1)
        : PlcToNusmvException("Invalid truth table file"), m_message(message),
          m_line(line) {}

    virtual std::string what() const {
        std::ostringstream msg;
        msg << PlcToNusmvException::what() << ". " << m_message;
        if (m_line > 0)
            msg << " at line " << m_line;

        return msg.str();
    }

private:
    std::string m_message;
    long m_line;
};

class InvalidStandardPouException : public PlcToNusmvException {
public:
    InvalidStandardPouException(const std::string & pou_name)
        : PlcToNusmvException("Invalid standard POU type"),
          m_pou_name(pou_name) {}

    virtual std::string what() const {
        std::ostringstream msg;
        msg << PlcToNusmvException::what() << ": " << m_pou_name;
        return msg.str();
    }
private:
    std::string m_pou_name;
};

class InvalidVariableException : public PlcToNusmvException {
public:
    InvalidVariableException(const std::string & message)
        : PlcToNusmvException("Invalid variable"), m_message(message) {}

    virtual std::string what() const {
        std::ostringstream msg;
        msg << PlcToNusmvException::what() << ". " << m_message;
        return msg.str();
    }
private:
    std::string m_message;
};

class InvalidNodeException : public PlcToNusmvException {
public:
    InvalidNodeException(const std::string & message)
        : PlcToNusmvException("Invalid node"), m_message(message) {}

    virtual std::string what() const {
        std::ostringstream msg;
        msg << PlcToNusmvException::what() << ". " << m_message;
        return msg.str();
    }
private:
    std::string m_message;
};

class GeneralASTBuilderException: public PlcToNusmvException {
public:
    GeneralASTBuilderException(const std::string & message)
        : PlcToNusmvException("AST Building error"), m_message(message) {}

    virtual std::string what() const {
        std::ostringstream msg;
        msg << PlcToNusmvException::what() << ". " << m_message;
        return msg.str();
    }
private:
    std::string m_message;
};

} // namespace plctonusm

#endif // EXEPTS_H
