/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <iostream>

#include "plctonusmv.h"
#include "plcreader.h"
#include "excepts.h"

#include "nusmvcodegenerator.h"

namespace plctonusmv {

void PlcToNusmv::show_file_names() const {
    using std::cout;
    using std::endl;

    cout << "PLC xml file: " << m_plc_xml_file << endl;
    cout << "Truthtable file: " << m_truthtable_file << endl;
    cout << "NUSM file: " << m_nusmv_file << endl;
}

void PlcToNusmv::process_input_files() {
    POU main_pou = build_main_pou();
    if (m_truthtable_file.size() > 0) {
        TruthTable tt = build_truth_table();
        compile_to_nusm(&main_pou, &tt);
    } else {
        compile_to_nusm(&main_pou);
    }
}

void PlcToNusmv::compile_to_nusm(const POU * main_pou, const TruthTable * tt) {
    ASTBuilder ast_builder(main_pou, m_optimize, tt);
    nusmvast::Program * program = ast_builder.build_ast();
    create_nusmv_file(program);
    delete program;
}

void PlcToNusmv::generate_test_case(size_t inputs, size_t outputs) {
    TestCaseGenerator test(inputs, outputs);
    POU main_pou = test.generate_fbd();
    ASTBuilder ast_builder(&main_pou, m_optimize);
    const nusmvast::Program * program = ast_builder.build_ast();
    create_nusmv_file(program);
    delete program;
}

// Private methods
POU PlcToNusmv::build_main_pou() {
    PlcReader plc_reader(m_plc_xml_file, m_xml_schema);
    POU main_pou = plc_reader.generate_fbd();

    if (plc_reader.has_errors()) {
        for (const std::string & str : plc_reader.get_errors())
            m_logger.warn(str);
    }

    return main_pou;
}

TruthTable PlcToNusmv::build_truth_table() {
    TruthTableReader ttr(m_truthtable_file);
    return ttr.generate_truth_table();
}

void PlcToNusmv::create_nusmv_file(const nusmvast::Program * program) const {
    if (m_nusmv_file.size() > 0) {
        std::ofstream fout(m_nusmv_file);
        if (fout.good()) {
            NusmvCodeGenerator cg(fout);
            cg.visit(program);
        } else
            throw PlcToNusmvException("Could not write to file/output stream.");
    } else {
        NusmvCodeGenerator cg(std::cout);
        cg.visit(program);
    }
}

} // namespace plctonusmv
