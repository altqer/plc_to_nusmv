/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include "plcvariable.h"

namespace plctonusmv {

PlcVariable::PlcVariable() {
    m_name = "var";
    m_type = "input";
    m_plc_type = "boolean";
}

PlcVariable::PlcVariable(const PlcVariable &var) {
    m_name = var.get_name();
    m_type = var.get_type();
    m_plc_type = var.get_plc_type();
}

PlcVariable::PlcVariable(const std::string &name, const std::string &type,
                         const std::string &plc_type)
{
    m_name = name;
    m_type = type;
    m_plc_type = plc_type;
}

} // namespace plctonusmv
