/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <QFile>
#include <QXmlSchema>
#include <QXmlSchemaValidator>

#include <iostream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <algorithm>

#include "plcreader.h"
#include "excepts.h"

namespace plctonusmv {

PlcReader::PlcReader(const string & xml_file,
                     const string & schema_file)
    : m_xml_file(xml_file), m_schema_file(schema_file)
{    
    if (! validate_xml())
        throw PlcXmlSchemaValidatorException(m_xml_file);
}

bool PlcReader::validate_xml() {
    if (m_schema_file.size() == 0)
        return true; // < Use Qt Resources >

    QUrl schema_url(QUrl::fromLocalFile(m_schema_file.c_str()));
    QXmlSchema schema;
    schema.load(schema_url);
    schema_url.clear();

    if (schema.isValid()) {
        QFile file(m_xml_file.c_str());
        file.open(QIODevice::ReadOnly);
        QXmlSchemaValidator validator(schema);
        return validator.validate(&file, QUrl::fromLocalFile(file.fileName()));
    }

    return false;
}

POU PlcReader::generate_fbd() {
    POU pou;
    read_plc_from_xml(pou);
    return pou;
}

void PlcReader::read_plc_from_xml(POU & pou) {
    QXmlStreamReader xml;

    QFile file(m_xml_file.c_str());
    file.open(QIODevice::ReadOnly);
    xml.setDevice(&file);

    xml.readNextStartElement();
    if (xml.name() != "project") {
        throw InvalidPlcXmlException("Element 'project' is missing",
                                     xml.lineNumber(), xml.columnNumber());
    }

    while(xml.readNextStartElement()) {
        if (xml.name() == "types") {
            while(xml.readNextStartElement()) {
                if (xml.name() == "pous") {
                    xml.readNextStartElement();
                    if (xml.name() != "pou") {
                        throw InvalidPlcXmlException(
                                    "Element 'pou' is missing",
                                    xml.lineNumber(), xml.columnNumber());
                    }

                    if (xml.attributes().value("pouType") != "program") {
                        throw InvalidPlcXmlException(
                                    "Attribute pouType is not 'program'",
                                    xml.lineNumber(), xml.columnNumber());
                    }

                    string fbd_name = xml.attributes().value("name")
                            .toString().toStdString();
                    pou.set_type(fbd_name);

                    while (xml.readNextStartElement()) {
                        if (xml.name() == "interface") {
                            xml.skipCurrentElement();
                        } else if (xml.name() == "body") {
                            read_pou_body(xml, pou);
                        } else
                            xml.skipCurrentElement();
                    }
                } else {
                    xml.skipCurrentElement();
                }
            } // end of while
        } else {
            xml.skipCurrentElement();
        }
    } // end of for
} // end of method read_plc_from_xml()

std::map<string, vector<PlcVariable>> PlcReader::read_all_variables(
        QXmlStreamReader & xml) const
{
    std::map<string, vector<PlcVariable>> vars;

    while (xml.readNextStartElement()) {
        if (xml.name() == "inputVars") {
            vars.insert( {"input", read_variables(xml, "input") } );
        } else if (xml.name() == "outputVars") {
            vars.insert( {"output", read_variables(xml, "output") } );
        } else if (xml.name() == "inOutVars") {
            vars.insert( {"in_out", read_variables(xml, "in_out") } );
        } else {
            xml.skipCurrentElement();
        }
    }

    return vars;
}

vector<PlcVariable> PlcReader::read_variables(QXmlStreamReader & xml,
                                              const string & type) const
{
    vector<PlcVariable> vars;

    while (xml.readNextStartElement()) {
        if (xml.name() == "variable") {
            string var_name = xml.attributes().value("name")
                    .toString().toStdString();
            xml.readNextStartElement();
            xml.readNextStartElement();
            if (xml.name() == "BOOL") {
                vars.push_back({var_name, type, "boolean"});
            }

            xml.skipCurrentElement();
            xml.skipCurrentElement();
            xml.skipCurrentElement();
        } else
            xml.skipCurrentElement();
    }

    return vars;
}

void PlcReader::read_pou_body(QXmlStreamReader & xml, POU & pou) {
    while (xml.readNextStartElement()) {
        if (xml.name() == "FBD") {
            read_fbd(xml, pou);
        } else
            xml.skipCurrentElement();
    }
}

void PlcReader::read_fbd(QXmlStreamReader &xml, POU & pou) {
    std::map<int, Node> nodes;
    std::map<string, int> var_node_map;

    std::map<string, size_t> gate_count;

    while (xml.readNextStartElement()) {
        if (xml.name() == "inVariable") {
            Node node = read_fbd_in_variable(xml);
            int local_id = node.get_local_id();
            string node_name = node.get_node_name();
            pou.insert_input_variable({node_name, "input"});
            nodes.insert( {local_id, node} );
            var_node_map.insert( {node_name, local_id} );
        } else if (xml.name() == "outVariable") {
            long line_number = xml.lineNumber(); // Used for error reporting
            Node node = read_fbd_out_variable(xml);
            if (node.get_nr_of_edges() > 0) {
                int local_id = node.get_local_id();
                string node_name = node.get_node_name();
                pou.insert_output_variable({node_name, "output"});
                nodes.insert( {local_id, node} );
                var_node_map.insert( {node_name, local_id} );
            } else {
                std::ostringstream ss;
                ss << "Output variable '" << node.get_node_name() << "'"
                   << " doesn't have any expression or value assigned to it"
                   << " at line " << line_number;
                m_errors.push_back(ss.str());
            }
        } else if (xml.name() == "block") {
            string type_name = xml.attributes().value("typeName")
                    .toString().toStdString();
            long line_number = xml.lineNumber(); // Used for error reporting

            Node node = read_fbd_block(xml);
            if (POU::is_std_pou_type(type_name)) {
                gate_count[type_name]++;
            }
            size_t nr_of_edges = node.get_nr_of_edges();
            size_t nr_of_inputs = node.get_pou()->get_input_variables().size();
            if (nr_of_edges == nr_of_inputs) {
                int local_id = node.get_local_id();
                nodes.insert( {local_id, node} );
            } else if (nr_of_edges > 0) {
                std::ostringstream ss;
                ss << "Block of type '" << type_name << "'"
                   << " has at least one input variable that is not connected"
                   << " to any variables or other blocks";
                throw InvalidPlcXmlException(ss.str(), line_number);
            } else {
                std::ostringstream ss;
                ss << "Block of type '" << type_name << "'"
                   << " is not connected to any variable"
                   << " at line " << line_number;
                m_errors.push_back(ss.str());
            }

        } else
            xml.skipCurrentElement();
    }

    std::map<string, size_t> gate_index;

    for (auto & pair : nodes) {
        if (pair.second.get_node_type() == POU_INSTANCE) {
            string type_name = pair.second.get_pou()->get_type();
            if (POU::is_std_pou_type(type_name)) {
                string gate_prefix = type_name;
                std::transform(gate_prefix.begin(),
                    gate_prefix.end(), gate_prefix.begin(), ::tolower);
                gate_prefix += "_gate";
                string var_name = generate_var_name(gate_prefix,
                                                    gate_count.at(type_name),
                                                    gate_index[type_name]++);
                pair.second.set_node_name(var_name);
                var_node_map.insert( {var_name, pair.second.get_local_id()} );
            }
        }

    }

    pou.insert_graph({nodes});
    pou.insert_var_node_map(var_node_map);
    //pou.show_graph_info();
} // end of method read_fbd()

Node PlcReader::read_fbd_in_variable(QXmlStreamReader & xml) const
{

    int local_id = xml.attributes().value("localId").toInt();
    string var_name;
    while (xml.readNextStartElement()) {
        if (xml.name() == "expression") {
            var_name = xml.readElementText().toStdString();
        } else
            xml.skipCurrentElement();
    }

    Node node(local_id, VARIABLE, PlcVariable(var_name, "input"));
    return node;
}

Node PlcReader::read_fbd_out_variable(QXmlStreamReader & xml) const {
    int local_id = xml.attributes().value("localId").toInt();
    string var_name;
    int ref_id = -1;
    while (xml.readNextStartElement()) {
        //string xml_name = xml.name().toString().toStdString();
        if (xml.name() == "expression") {
            var_name = xml.readElementText().toStdString();
        } else if (xml.name() == "connectionPointIn") {
            ref_id = read_connection_point_in(xml);
            if (ref_id != -1)
                xml.skipCurrentElement();
        } else
            xml.skipCurrentElement();
    }

    Node node(local_id, VARIABLE, PlcVariable(var_name, "output"));
    if (ref_id != -1)
        node.insert_edge( {ref_id} );
    return node;
}

Node PlcReader::read_fbd_block(QXmlStreamReader & xml) const
{
    int local_id = xml.attributes().value("localId").toInt();
    string type_name = xml.attributes().value("typeName")
            .toString().toStdString();

    POU std_pou;

    if (POU::is_std_pou_type(type_name)) {
        std_pou = POU::get_standard_pou(type_name);
    } else {
        throw InvalidPlcXmlException("Unsupported PLC block type: " + type_name,
                    xml.lineNumber());
    }

    vector<Edge> edges;
    size_t nr_of_input_variables = 0;

    while (xml.readNextStartElement()) {
        if (xml.name() == "inputVariables") {
            while(xml.readNextStartElement()) {
                if (xml.name() == "variable") {
                    ++nr_of_input_variables;
                    int ref_id = -1;
                    string formal_parameter = xml.attributes()
                            .value("formalParameter").toString().toStdString();
                    while(xml.readNextStartElement()) {
                        if (xml.name() == "connectionPointIn") {
                            ref_id = read_connection_point_in(xml);
                        } else
                            xml.skipCurrentElement();
                    }

                    if (ref_id != -1) {
                        edges.push_back( {ref_id, formal_parameter} );
                        xml.skipCurrentElement();
                    }
                } else
                    xml.skipCurrentElement();
            }
        } else
            xml.skipCurrentElement();
    } // end of while

    if (type_name != "NOT") {
        for (size_t i = 3; i <= nr_of_input_variables; ++i) {
            std::ostringstream ss;
            ss << "IN" << i;
            std_pou.insert_input_variable(ss.str());
        }
    }

    Node node(local_id, POU_INSTANCE, std_pou, "");

    for (const Edge & edge : edges)
        node.insert_edge(edge);

    return node;
} // end of method read_fbd_block()

int PlcReader::read_connection_point_in(QXmlStreamReader &xml) const {
    if (xml.name() != "connectionPointIn")
        return -1;

    int ref_id = -1;
    while(xml.readNextStartElement()) {
        if (xml.name() == "connection") {
            ref_id = xml.attributes().value("refLocalId").toInt();
            xml.skipCurrentElement();
            break;
        } else
            xml.skipCurrentElement();
    }

    return ref_id;
}

string PlcReader::generate_var_name(const std::string & prefix, size_t vars,
                                    size_t number)
{
    size_t leading_zeros = 0;
    size_t tmp = vars - 1;
    while (tmp >= 10) {
        leading_zeros++;
        tmp = tmp / 10;
    }

    std::ostringstream ss;

    if (leading_zeros++ > 0)
        ss << std::setfill('0');

    ss << prefix << std::setw(leading_zeros) << number;

    return ss.str();
}

} // namespace plctonusm
