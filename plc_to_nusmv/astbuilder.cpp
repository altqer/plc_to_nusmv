/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <cmath>
#include <algorithm>

#include "astbuilder.h"
#include "excepts.h"

namespace plctonusmv {

nusmvast::Program * ASTBuilder::build_ast() const {
    using namespace nusmvast;

    const vector<PlcVariable>  & input_vars = m_pou->get_input_variables();
    const vector<PlcVariable> & output_vars = m_pou->get_output_variables();

    if (input_vars.size() == 0) {
        throw GeneralASTBuilderException("No input variables found.");
    }

    if (output_vars.size() == 0) {
        throw GeneralASTBuilderException("No output variables found.");
    }

    TypeSpecifier bool_type("boolean");

    // Parameters used in the FBD and truth table module
    vector<ParamDecl> params;
    for (const PlcVariable & v : input_vars) {
        params.push_back(ParamDecl(v.get_name()));
    }

    // Output variables of the FBD and truth table module. They must be placed
    // in the VAR section of a NUSMV module
    vector<VarDecl> vars;
    for (const PlcVariable & v : output_vars) {
        vars.push_back(VarDecl(v.get_name(), bool_type));
    }
    // FBD module
    Module * fbd_module = create_fbd_module(params, vars);

    // Truth table
    Module * truth_table_module = nullptr;
    if (!m_optimize) {
        std::unordered_map< string, std::pair<bool, vector<vector<bool>>> >
                truth_table;
        if (m_tt)
            truth_table = build_truth_table();
        else
            truth_table = build_truth_table_from_ast();

        // TruthTable module
        truth_table_module = create_truth_table_module(params, vars,
                                                       truth_table);
    }

    // Main module
    Module * main_module = create_main_module(fbd_module, truth_table_module);

    // Creating the program (root node of the AST)
    Program * program = new Program();
    program->insert_module(fbd_module);
    program->insert_module(truth_table_module);
    program->insert_module(main_module);

    return program;
}

nusmvast::Module * ASTBuilder::create_fbd_module(
        const vector<nusmvast::ParamDecl> & params,
        const vector<nusmvast::VarDecl> & vars
) const
{
    using namespace nusmvast;

    const vector<PlcVariable> & output_vars = m_pou->get_output_variables();
    const std::map<string, int>  &var_node_map = m_pou->get_var_node_map();

    LiteralExpr lit_false("boolean", "FALSE");

    Module * fbd_module = new Module(m_pou->get_type(), params);

    for (const VarDecl & vd : vars) {
        fbd_module->insert_var(vd);
    }

    // Define section
    const FBDGraph & graph = m_pou->get_graph();
    for (const std::pair<int, Node> & par : graph.get_nodes()) {
        if (par.second.get_node_type() != VARIABLE)
            fbd_module->insert_define(create_define_decl(par.second));
    }

    // Asign section

    if (! m_optimize) {
        for (const PlcVariable & var : output_vars) {
            string var_name = var.get_name();
            AssignDecl assign_decl_init_fbd(AssignDecl::INIT,
                                        VarExpr(var_name), lit_false);
            fbd_module->insert_assign(assign_decl_init_fbd);
        }
    }

    AssignDecl::assign_type_enum assign_type = m_optimize ?
                AssignDecl::STANDARD : AssignDecl::NEXT;

    for (const PlcVariable & var : output_vars) {
        string var_name = var.get_name();

        int local_id = var_node_map.find(var_name)->second;
        const Node * node = graph.get_node(local_id);
        if (node == nullptr || node->get_node_type() == POU_INSTANCE)
            continue; // Just in case

        const vector<Edge> & edges = node->get_edges();

        if (edges.size() != 1)
            continue; // OOPS! Output variables can only have one edge

        int ref_id = edges[0].get_ref_local_id();
        const Node * ref_node = graph.get_node(ref_id);
        string ref_name = ref_node->get_node_name();

        const AssignDecl assign_decl_next_fbd(assign_type,
                                              VarExpr(var_name),
                                              VarExpr(ref_name));

        fbd_module->insert_assign(assign_decl_next_fbd);
    }

    return fbd_module;
} // end of method create_fbd_module()

nusmvast::Module * ASTBuilder::create_truth_table_module(
        const vector<nusmvast::ParamDecl> & params,
        const vector<nusmvast::VarDecl> & vars,
        const std::unordered_map<std::string,
            std::pair<bool, vector<vector<bool> > > > & truth_table
) const
{
    using namespace nusmvast;

    const vector<PlcVariable> & input_vars = m_pou->get_input_variables();
    const vector<PlcVariable> & output_vars = m_pou->get_output_variables();

    LiteralExpr lit_false("boolean", "FALSE");

    Module * truth_table_module = new Module("TruthTable", params);

    // VAR secion
    for (const VarDecl & vd : vars)
        truth_table_module->insert_var(vd);

    // Assign section
    for (const PlcVariable & var : output_vars) {
        string var_name = var.get_name();
        AssignDecl assign_init(AssignDecl::INIT, VarExpr(var_name), lit_false);
        truth_table_module->insert_assign(assign_init);
    }

    for (const PlcVariable & var : output_vars) {
        string var_name = var.get_name();
        auto it = truth_table.find(var_name);
        if (it == truth_table.end())
            continue;
        CaseExpr case_expr_tt = build_truth_table_case_expr(
                    it->second.first,
                    it->second.second,
                    input_vars);
        AssignDecl assign_next(AssignDecl::NEXT, VarExpr(var_name),
                               case_expr_tt);
        truth_table_module->insert_assign(assign_next);
    }

    return truth_table_module;
} // end of method create_truth_table_module()

nusmvast::Module * ASTBuilder::create_main_module(
        const nusmvast::Module * fbd_module,
        const nusmvast::Module * truth_table_module
) const
{
    using namespace nusmvast;

    const vector<PlcVariable> & input_vars = m_pou->get_input_variables();
    const vector<PlcVariable> & output_vars = m_pou->get_output_variables();

    string fbd_name = m_pou->get_type();
    std::transform(fbd_name.begin(), fbd_name.end(), fbd_name.begin(),
                   ::tolower);
    std::replace(fbd_name.begin(), fbd_name.end(), ' ', '_');
    string truth_table_name = "truth_table";

    TypeSpecifier bool_type("boolean");
    LiteralExpr lit_false("boolean", "FALSE");
    LiteralExpr lit_true ("boolean", "TRUE");

    Module * main_module = new Module("main");

    // VAR section
    for (const PlcVariable & var : input_vars)
        main_module->insert_var(VarDecl(var.get_name(), bool_type));

    TypeSpecifier fbd_module_type = fbd_module->get_type_specifier();
    main_module->insert_var(VarDecl(fbd_name, fbd_module_type));

    if (m_optimize) {
        // DEFINE section
        for (const PlcVariable & var : output_vars) {
            Expression * formula_ast = build_formula_ast(var);
            DefineDecl dd(var.get_name() + "_exp", formula_ast);
            main_module->insert_define(dd);
        }
    } else {
        TypeSpecifier truth_table_module_type =
                truth_table_module->get_type_specifier();

        main_module->insert_var(VarDecl(truth_table_name,
                                        truth_table_module_type));
    }

    // ASSIGN section
    SetExpr expr_set;
    expr_set.insert_expr(&lit_false);
    expr_set.insert_expr(&lit_true);

    for (const PlcVariable & var : input_vars) {
        main_module->insert_assign(AssignDecl(AssignDecl::INIT,
            VarExpr(var.get_name()), expr_set));
    }

    for (const PlcVariable & var : input_vars) {
        main_module->insert_assign(AssignDecl(AssignDecl::NEXT,
            VarExpr(var.get_name()), expr_set));
    }

    // LTL specification
    if (output_vars.size() > 0) {
         LTLExpr ltl_expr_outer = create_ltl_expr(fbd_name, truth_table_name,
                                                  output_vars);
         main_module->insert_ltl(ltl_expr_outer);
    }

    return main_module;
} // end of method create_main_module()

std::unordered_map< string, std::pair<bool, vector<vector<bool>>> >
    ASTBuilder::build_truth_table() const
{
    using namespace nusmvast;

    std::unordered_map< string,
            std::pair<bool, vector<vector<bool>>> > truth_table;

    const vector<PlcVariable> & output_vars = m_pou->get_output_variables();
    const vector<PlcVariable>  & input_vars = m_pou->get_input_variables();

    size_t in_vars_count = input_vars.size();
    size_t out_vars_count = output_vars.size();

    // Compare sizes with m_tt, throw an exception if different
    if (m_tt->vars.size() != in_vars_count + out_vars_count) {
        string msg = "The number of variables from the PLC and truth table "
            "file doesn't match";
        throw TruthTableException(msg);
    }

    for (const PlcVariable & v : output_vars) {
        vector<vector<bool>> tt_false_vector;
        vector<vector<bool>> tt_true_vector;
        size_t var_index = 0;

        auto it = m_tt->var_map.find(v.get_name());
        if (it != m_tt->var_map.end()) {
            var_index = it->second;
        } else {
            string msg = "The variable '" + v.get_name()
                + " was not found in the truth table";
            throw TruthTableException(msg);
        }

        for (size_t i = 0; i < m_tt->tt.size(); ++i) {
            vector<bool> bool_vector;

            for (size_t j = 0; j < in_vars_count; ++j) {
                bool value = m_tt->tt.at(i).at(j);
                bool_vector.push_back(value);
            }

            if (m_tt->tt.at(i).at(var_index))
                tt_true_vector.push_back(bool_vector);
            else
                tt_false_vector.push_back(bool_vector);
        }

        if (tt_true_vector.size() < tt_false_vector.size())
            truth_table.insert( {v.get_name(), {true, tt_true_vector}} );
        else
            truth_table.insert( {v.get_name(), {false, tt_false_vector}} );
    }

    return truth_table;
} // end of method build_truth_table()

std::unordered_map< string, std::pair<bool, vector<vector<bool>>> >
    ASTBuilder::build_truth_table_from_ast() const
{
    using namespace nusmvast;

    std::unordered_map< string,
            std::pair<bool, vector<vector<bool>>> > truth_table;

    const vector<PlcVariable> & output_vars = m_pou->get_output_variables();
    const vector<PlcVariable>  & input_vars = m_pou->get_input_variables();

    size_t in_vars_count = input_vars.size();

    for (const PlcVariable & var : output_vars) {
        Expression * formula_ast = build_formula_ast(var);
        vector<vector<bool>> tt_false_vector;
        vector<vector<bool>> tt_true_vector;

        for (size_t i = 0; i < std::pow(2, in_vars_count); ++i) {
            std::unordered_set<string> true_input_vars;

            vector<bool> bool_vector;
            for (size_t j = 0; j < in_vars_count; ++j) {
                bool value = (i >> (in_vars_count - j - 1)) & 1;
                bool_vector.push_back(value);

                if (value)
                    true_input_vars.insert(input_vars[j].get_name());
            }

            if (check_formula(formula_ast, true_input_vars))
                tt_true_vector.push_back(bool_vector);
            else
                tt_false_vector.push_back(bool_vector);
        }

        if (tt_true_vector.size() < tt_false_vector.size())
            truth_table.insert( {var.get_name(), {true, tt_true_vector}} );
        else
            truth_table.insert( {var.get_name(), {false, tt_false_vector}} );

        delete formula_ast;
    }


    return truth_table;
} // end of method build_truth_table()

nusmvast::Expression * ASTBuilder::build_formula_ast(const PlcVariable & var)
    const
{
    using namespace nusmvast;

    const std::map<string, int> & var_node_map = m_pou->get_var_node_map();
    const FBDGraph & graph = m_pou->get_graph();

    auto it = var_node_map.find(var.get_name());
    if (it == var_node_map.end()) {
        string msg = "Unknown output variable '" + var.get_name() + "'";
        throw InvalidVariableException(msg);
    }

    Expression * expr = nullptr;

    const Node * node = graph.get_node(it->second);
    const vector<Edge> & edges = node->get_edges();

    // Only variables with one edge are valid output variables
    if (edges.size() == 1 && node->get_node_type() == VARIABLE) {
        int ref_id = edges[0].get_ref_local_id();
        const Node * ref_node = graph.get_node(ref_id);
        expr = build_formula_ast_rec(ref_node);
    } else {
        // This should never happen
        string msg = "Output variable '" + var.get_name()
            + "' doesn't have exactly one incoming edge.";
        throw InvalidVariableException(msg);
    }

    return expr;
}

nusmvast::Expression * ASTBuilder::build_formula_ast_rec(const Node * node)
    const
{
    using namespace nusmvast;

    Expression * expr = nullptr;

    if (node->get_node_type() == POU_INSTANCE) {
        const vector<Edge> & edges = node->get_edges();
        const FBDGraph & graph = m_pou->get_graph();
        const POU * pou = node->get_pou();
        string symbol = get_symbol(*pou);

        if (edges.size() == 1) {
            int ref_id = edges.at(0).get_ref_local_id();
            const Node * ref_node = graph.get_node(ref_id);
            Expression * tmp = build_formula_ast_rec(ref_node);
            expr = new OpExpr("boolean", symbol, *tmp);
            delete tmp;
        } else {
            for (const Edge & edge : edges) {
                int ref_id = edge.get_ref_local_id();
                const Node * ref_node = graph.get_node(ref_id);
                Expression * tmp = build_formula_ast_rec(ref_node);

                if (! expr) {
                    expr = tmp;
                } else {
                    Expression * new_op;
                    new_op = new OpExpr("boolean", symbol, *expr, *tmp);
                    delete tmp;
                    delete expr;
                    expr = new_op;
                }
            }
        }
    } else {
        expr = new VarExpr(node->get_node_name());
    }

    return expr;
}

bool ASTBuilder::check_formula(
        const nusmvast::Expression * expr,
        const std::unordered_set<string> & true_input_vars
) const
{
    using namespace nusmvast;

    if (dynamic_cast<const VarExpr*>(expr)) {
        const VarExpr * var_expr = dynamic_cast<const VarExpr*>(expr);
        auto it = true_input_vars.find(var_expr->get_identifier());
        if (it != true_input_vars.end())
            return true;
    } else if (dynamic_cast<const LiteralExpr*>(expr)) {
        const LiteralExpr * lit_expr = dynamic_cast<const LiteralExpr*>(expr);
        if (lit_expr->get_literal() == "TRUE")
            return true;
    } else if (dynamic_cast<const OpExpr*>(expr)) {
        const OpExpr * op_expr = dynamic_cast<const OpExpr*>(expr);
        const Expression * left_expr = op_expr->get_left_expression();
        const Expression * right_expr = op_expr->get_right_expression();
        string symbol = op_expr->get_symbol();
        if (symbol == "&") {
            return check_formula(left_expr, true_input_vars) &&
                    check_formula(right_expr, true_input_vars);
        } else if (symbol == "|") {
            return check_formula(left_expr, true_input_vars) ||
                    check_formula(right_expr, true_input_vars);
        } else if (symbol== "xor") {
            return check_formula(left_expr, true_input_vars) ^
                    check_formula(right_expr, true_input_vars);
        } else if (symbol == "!") {
            return ! check_formula(left_expr, true_input_vars);
        } else {
            string msg = "Unknown symbol: " + symbol;
            throw GeneralASTBuilderException(msg);
        }
    }

    return false;
}

const nusmvast::DefineDecl ASTBuilder::create_define_decl(const Node &node)
    const
{
    using namespace nusmvast;

    const FBDGraph & graph = m_pou->get_graph();
    const vector<Edge> & edges = node.get_edges();
    const POU * pou = node.get_pou();
    const string symbol = get_symbol(*pou);

    Expression * expr = nullptr;

    if (edges.size() > 1) {
        for (const Edge & edge : edges) {
            int ref_id = edge.get_ref_local_id();
            const Node * ref_node = graph.get_node(ref_id);
            string ref_name = ref_node->get_node_name();

            if (! expr) { // First variable
                expr = new VarExpr(ref_name);
            } else {
                Expression * tmp;
                Expression * new_op;

                tmp = new VarExpr(ref_name);
                new_op = new OpExpr("boolean", symbol, *expr, *tmp);
                delete expr;
                delete tmp;
                expr = new_op;
            }
        }
    } else if (edges.size() == 1) {
        int ref_id = edges.at(0).get_ref_local_id();
        const Node * ref_node = graph.get_node(ref_id);
        string ref_name = ref_node->get_node_name();
        Expression * tmp = new VarExpr(ref_name);
        expr = new OpExpr("boolean", symbol, *tmp);
        delete tmp;
    } else {
        string msg = "Node '" + node.get_node_name()
            + "' doesn't have any incoming edge.";
        throw InvalidNodeException(msg);
    }

    DefineDecl define_decl(node.get_node_name(), expr);
    delete expr;
    return define_decl;
}

const string ASTBuilder::get_symbol(const POU & pou) const {
    if (pou.get_type() == "AND")
        return "&";
    else if (pou.get_type() == "OR")
        return "|";
    else if (pou.get_type() == "XOR")
        return "xor";
    else if (pou.get_type() == "NOT")
        return "!";
    else
        return "";
}

nusmvast::CaseExpr ASTBuilder::build_truth_table_case_expr(
        bool tt_true,
        const vector<vector<bool> > & truth_table,
        const vector<PlcVariable> & input_vars
) const
{
    using namespace nusmvast;

    std::vector<string> tt_vars;
    tt_vars.reserve(input_vars.size());
    for (const PlcVariable & var : input_vars)
        tt_vars.push_back(var.get_name());
    CaseExpr truth_table_case_expr(truth_table, tt_vars, tt_true);

    /*
    LiteralExpr lit_false("boolean", "FALSE");
    LiteralExpr lit_true("boolean", "TRUE");

    CaseExpr truth_table_case_expr;
    Expression * prev; // Previous expression

    for (size_t i = 0; i < truth_table.size(); ++i) {
        prev = nullptr;

        for (size_t j = 0; j < truth_table[i].size(); ++j) {
            if (! prev) { // First variable
                if (truth_table[i][j])
                    prev = new VarExpr(input_vars[j].get_name());
                else
                    prev = new OpExpr("boolean", "!", VarExpr(
                                          input_vars[j].get_name()));
            } else {
                Expression * tmp;
                Expression * new_op;
                if (truth_table[i][j])
                    tmp = new VarExpr(input_vars[j].get_name());
                else
                    tmp = new OpExpr("boolean", "!", VarExpr(
                                         input_vars[j].get_name()));

                new_op = new OpExpr("boolean", "&", *prev, *tmp);
                delete prev;
                delete tmp;
                prev = new_op;
            }
        }

        if (tt_true)
            truth_table_case_expr.insert_case(prev, &lit_true);
        else
            truth_table_case_expr.insert_case(prev, &lit_false);

        delete prev;
    }

    if (tt_true)
        truth_table_case_expr.insert_case(&lit_true, &lit_false);
    else
        truth_table_case_expr.insert_case(&lit_true, &lit_true);
    */

    return truth_table_case_expr;
}

const nusmvast::LTLExpr ASTBuilder::create_ltl_expr(
        const string & fbd_name,
        const string & truth_table_name,
        const vector<PlcVariable> &output_vars
) const
{
    using namespace nusmvast;
    LTLExpr * ltl_expr_inner = nullptr;

    if (m_optimize) {
        for (const PlcVariable & var : output_vars) {
            string var_name = var.get_name();
            VarExpr fbd_var(fbd_name, VarExpr(var_name));
            string var_name_def = var_name + "_exp";
            VarExpr var_def(var_name_def);

            if (! ltl_expr_inner) { // First variable
                ltl_expr_inner = new LTLExpr(&fbd_var, &var_def, "<->");
            } else {
                LTLExpr tmp(&fbd_var, &var_def, "<->");
                LTLExpr * new_ltl = new LTLExpr(ltl_expr_inner, &tmp, "&");
                delete ltl_expr_inner;
                ltl_expr_inner = new_ltl;
            }
        }
    } else {
        for (const PlcVariable & var : output_vars) {
            string var_name = var.get_name();
            VarExpr fbd_var(fbd_name, VarExpr(var_name));
            VarExpr tt_var(truth_table_name, VarExpr(var_name));

            if (! ltl_expr_inner) { // First variable
                ltl_expr_inner = new LTLExpr(&fbd_var, &tt_var, "<->");
            } else {
                LTLExpr tmp(&fbd_var, &tt_var, "<->");
                LTLExpr * new_ltl = new LTLExpr(ltl_expr_inner, &tmp, "&");
                delete ltl_expr_inner;
                ltl_expr_inner = new_ltl;
            }
        }
    }

    LTLExpr ltl_expr_outer(ltl_expr_inner, "G");
    delete ltl_expr_inner;
    return ltl_expr_outer;
}

} // namespace plctonusmv
