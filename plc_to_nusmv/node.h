/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef NODE_H
#define NODE_H

#include <iostream>
#include <vector>

#include "pou.h"
#include "plcvariable.h"
#include "edge.h"

namespace plctonusmv {

class POU;

enum node_type {VARIABLE, POU_INSTANCE};

/*
 * The class that represents the Node element in the POU data structure. It
 * is placed in a graph of a POU.
 */
class Node
{
public:
    Node(int local_id, node_type type, const PlcVariable & var,
         const string & node_name = "");
    Node(int local_id, node_type type, const POU & pou,
         const string & node_name = "");
    Node(const Node & n);
    Node & operator=(const Node & n);
    ~Node();

    int get_local_id() const { return m_local_id; }
    string get_node_name() const { return m_node_name; }
    void set_node_name(const string & node_name) { m_node_name = node_name; }

    /*
     * Returns a value that tells you if the Node is either a variable or a
     * POU instance.
     */
    node_type get_node_type() const { return m_node_type; }

    /*
     * Returns the POU instance. nullptr might be returned if the Node is
     * actually a variable.
     */
    const POU * get_pou() const {  return m_pou; }

    /*
     * Returns the variable. nullptr might be returned if the Node is
     * actually a POU instance.
     */
    const PlcVariable * get_variable() const {  return m_var; }

    /*
     * Retunrs a reference to the list of incoming edges.
     */
    const std::vector<Edge> & get_edges() const { return m_in_edges; }

    // Insert edge between nodes
    void insert_edge(Edge e);

    size_t get_nr_of_edges() const { return m_in_edges.size(); }

    // Print Status. Debugging
    void show_info() const;
private:
    int m_local_id;
    string m_node_name;
    node_type m_node_type;
    std::vector<Edge> m_in_edges;
    PlcVariable * m_var;
    POU * m_pou;

    Node();
    void show_edges() const;
};

} // namespace plctonusm

#endif // NODE_H
