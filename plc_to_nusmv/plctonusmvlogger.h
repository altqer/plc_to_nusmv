/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#ifndef PLCTONUSMVLOGGER_H
#define PLCTONUSMVLOGGER_H

#include <sstream>
#include <string>

namespace plctonusmv {

/*
 * Used for outputting error/warning messages to an output stream.
 */
class PlcToNusmvLogger
{
public:
    PlcToNusmvLogger(std::ostream & os) : m_os(os) {}
    void warn(const std::string & msg);
    void error(const std::string & msg);
    void fatal_error(const std::string & msg);
private:
    std::ostream & m_os;
};

} // namespace plctonusm

#endif // PLCTONUSMVLOGGER_H
