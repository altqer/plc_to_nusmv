/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include <QCoreApplication>
#include <QDir>

#include <string>
#include <iostream>
#include <vector>
#include "unordered_map"
#include <iomanip>

#include "plctonusmv.h"
#include "excepts.h"
#include "plctonusmvlogger.h"

void start_terminal_program(int argc, char *argv[], QCoreApplication & q_app);
void read_args(int argc, char *argv[],
               std::string & plc_xml_file,
               std::string & truthtable_file,
               std::string & nusmv_file,
               std::string & xml_schema,
               bool & test, bool & optimize,
               int & nr_of_inputs, int & nr_of_outputs,
               plctonusmv::PlcToNusmvLogger & logger);
void print_arg_info(const string & arg = "");
std::string clean_arg(const string & arg);
std::string get_full_arg_name(const string & arg);
bool is_number(const string & str);
std::string get_app_dir();
bool validate_readable_file(const std::string & file);
void log_error_and_terminate(plctonusmv::PlcToNusmvLogger & logger,
                             const string & message);
void terminate_program(const string & message = "Aborting.");

const std::unordered_map<std::string, std::string> arg_list = {
    {"xml", "The XML file where the PLC code is saved in. (REQUIRED)"},
    {"truth-table", "The truth table file that will be used as a\n\t\t"
        "specifacation to the program. (REQUIRED)"},
    {"nusmv", "The path of the NUSMV file to generate."},
    {"schema", "The XML schema file that is used by the compiler. Only\n\t\t"
        "the PLC open XML schema 2.01 can be used currently."},
    {"test", "Generate a PLC program with a given number of input and\n\t\t"
        "output variables. There are restrictions on the number \n\t\t"
        "of variables and these are x >= 2, y >= 1 and x > y, \n\t\t"
        "where x and y are the number of input and output \n\t\t"
        "variables respectively."},
    {"optimize", "Optimize the Truth table module by building and\n\t\t"
        "using Boolean formulas instead of the actual truth table.\n\t\t"
        "The function will currently only work with --test."},
    {"help", "Prints information about all or just one specific \n\t\t"
        "argument."},
};

const std::unordered_map<std::string, std::string> arg_short = {
    {"x", "xml"},
    {"t", "truth-table"},
    {"n", "nusmv"},
    {"o", "optimize"},
};

const std::vector<std::string> arg_list_order = {
    "xml",
    "truth-table",
    "nusmv",
    "schema",
    "test",
    "optimize",
    "help",
};

const std::regex Re_arg(R"(-*([a-z]\w*))");


int main(int argc, char *argv[]) {
    QCoreApplication q_app(argc, argv);

    start_terminal_program(argc, argv, q_app);

    return 0;
    //return q_app.exec();
}

/*
 * Starts the program in the command-line interface.
 */
void start_terminal_program(int argc, char *argv[], QCoreApplication & q_app) {
    using std::cout;
    using std::endl;
    using std::string;

    plctonusmv::PlcToNusmvLogger logger(std::cerr);

    string app_dir = q_app.applicationDirPath().toStdString();
    string plc_xml_file, truthtable_file, nusmv_file, xml_schema;

    q_app.addLibraryPath(q_app.applicationDirPath());

    //plc_xml_file = "/home/altin/beremiz/beremiz_projects/case_study_p215/plc.xml";
    //truthtable_file = "/media/System_HDD/Documents/UiO/master_thesis/misc_files/fbd_to_nusm//truthtable_falcon.csv";    
    xml_schema = app_dir + "/xsd/tc6_xml_v201.xsd";

    bool test = false;
    bool optimize = false;
    int nr_of_inputs = 3;
    int nr_of_outputs = 1;

    read_args(argc, argv, plc_xml_file, truthtable_file, nusmv_file, xml_schema,
              test, optimize, nr_of_inputs, nr_of_outputs, logger);

    if (! test) {
        if (! validate_readable_file(plc_xml_file)) {
            logger.error("Could not read PLC XML file. "
                         "Type --help for usage information.");
            terminate_program();
        }

        if (! validate_readable_file(truthtable_file)) {
            logger.error("Could not read truth table file. "
                         "Type --help for usage information.");
            terminate_program();
        }

        if (! validate_readable_file(xml_schema)) {
            logger.error("Could not read OpenPLC XML schema file. "
                         "Type --help for usage information.");
            terminate_program();
        }
    }

    plctonusmv::PlcToNusmv plc(plc_xml_file, truthtable_file, nusmv_file,
                               xml_schema, optimize, logger);

    try {
        if (! test) {
            plc.process_input_files();
        } else {
            plc.generate_test_case(nr_of_inputs, nr_of_outputs);
        }
    } catch (const plctonusmv::PlcToNusmvException & e) {
        logger.error(e.what());
        terminate_program();
    } catch (const std::runtime_error & e) {
        logger.error(e.what());
        terminate_program();
    }
} // end of function start_terminal_app()

/*
 * Reads and analyzes the arguments given to the program from the command line.
 */
void read_args(int argc, char *argv[],
               std::string &plc_xml_file,
               std::string &truthtable_file,
               std::string &nusmv_file,
               std::string &xml_schema,
               bool & test,  bool & optimize,
               int &nr_of_inputs, int &nr_of_outputs,
               plctonusmv::PlcToNusmvLogger & logger)
{
    if (argc == 2 || argc == 3) {
        string arg = argv[1];
        string cleaned_arg = clean_arg(arg);
        string full_arg = get_full_arg_name(cleaned_arg);

        if (full_arg == "help") {
            if (argc == 2) {
                print_arg_info();
            } else { // argc == 3
                print_arg_info(argv[2]);
            }

            exit(EXIT_SUCCESS);
        }
    }

    for (int i = 1; i < argc; ++i) {
        string arg = argv[i];
        string cleaned_arg = clean_arg(arg);
        string full_arg = get_full_arg_name(cleaned_arg);

        if (full_arg == "xml") {
            if (i < argc - 1)
                plc_xml_file = argv[i++ + 1];
            else {
                string error = "Invalid argument to " + arg
                        + ". Use --help for more information.";
                log_error_and_terminate(logger, error);
            }
        } else if (full_arg == "truth-table") {
            if (i < argc - 1)
                truthtable_file = argv[i++ + 1];
            else {
                string error = "Invalid argument to " + arg
                        + ". Use --help for more information.";
                log_error_and_terminate(logger, error);
            }
        } else if (full_arg == "nusmv") {
            if (i < argc - 1)
                nusmv_file = argv[i++ + 1];
            else {
                string error = "Invalid argument to " + arg
                        + ". Use --help for more information.";
                log_error_and_terminate(logger, error);
            }
        } else if (full_arg == "schema") {
            if (i < argc - 1)
                xml_schema = argv[i++ + 1];
            else {
                string error = "Invalid argument to " + arg
                        + ". Use --help for more information.";
                log_error_and_terminate(logger, error);
            }
        } else if (full_arg == "test") {
            if (i >= argc - 2) {
                string error = "Invalid argument to " + arg + ". Format should"
                        " be --test x y, where x is the number of input "
                        "variables and y the number of output variables.";
                log_error_and_terminate(logger, error);
            }

            if (!is_number(argv[i + 1]) || !is_number(argv[i + 2])) {
                string error = "Invalid numbers for the input and output "
                        "variables.";
                log_error_and_terminate(logger, error);
            }

            nr_of_inputs =  atoi(argv[i + 1]);
            nr_of_outputs = atoi(argv[i + 2]);
            if (nr_of_inputs < 2 || nr_of_outputs < 1 ||
                    nr_of_inputs <= nr_of_outputs) {
                string error = "Invalid input and output operators. Should be"
                        " x >= 2, y >= 1 and x > y where x and y are the number"
                        " of input and output variables respectively.";
                log_error_and_terminate(logger, error);
            }

            test = true;
            i += 2;
        } else if (full_arg == "optimize") {
            optimize = true;
        }
    } // end of for
} // end of function read_args()


/*
 * Prints information about the arguments that the program can accept. Will
 * print information about all arguments if no value is given as argument to the
 * function.
 */
void print_arg_info(const std::string & arg) {
    const char separator = ' ';
    const size_t col_width = 16;

    bool print_all = arg.size() == 0;

    if (print_all) {
        for (const std::string a : arg_list_order) {
            std::cout << std::setfill(separator) << std::setw(col_width)
                      << std::left << "--" + a
                      << std::setw(col_width) << arg_list.at(a)
                      << std::endl;
        }
    } else {
        auto it = arg_list.find(arg);
        if (it != arg_list.end()) {
            std::cout << std::setfill(separator) << std::setw(col_width)
                      << std::left << "--" + it->first
                      << std::setw(col_width) << it->second
                      << std::endl;

        } else {
            std::cerr << "Invalid argument." << std::endl;
        }
    }
}

/*
 * Takes the argument given to the program and removes any preceding dashes. For
 * example, the argument '-x' will return 'x', '--xml' will return 'xml'.
 */
std::string clean_arg(const std::string & arg) {
    std::sregex_token_iterator re_it_args(arg.begin(), arg.end(), Re_arg, 1);
    std::sregex_token_iterator re_it_end;

    if (re_it_args != re_it_end)
        return *re_it_args;
    else
        return arg;
}

/*
 * Returns the full argument name if the short name does exist. Returns the
 * same string if it does not.
 */
std::string get_full_arg_name(const std::string & arg) {
    auto it = arg_short.find(arg);
    if (it != arg_short.end())
        return it->second;
    else
        return arg;
}

/**
 * Returns true if the string is a number and false otherwise.
 */
bool is_number(const std::string & str) {
    return !str.empty() &&
            str.find_first_not_of("0123456789") == std::string::npos;
}

/*
 * Checs if the the file given as argument can be read by trying to open it.
 */
bool validate_readable_file(const std::string & file) {
    std::ifstream f(file.c_str());
    if (f.is_open()) {
        f.close();
        return true;
    } else
        return false;
}

/*
 * Sends the error message in the second argument to the logger object given as
 * argument and calls terminate_program. The logger object prints the error
 * message.
 */
void log_error_and_terminate(plctonusmv::PlcToNusmvLogger & logger,
                             const string & message)
{
    logger.error(message);
    terminate_program();
}

void terminate_program(const string & message) {
    std::cerr << message << std::endl;
    exit(EXIT_FAILURE);
}

