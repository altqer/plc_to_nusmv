/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include "fbdgraph.h"

namespace plctonusmv {

void FBDGraph::show_info() const {
    for (const std::pair<int, Node> & node : m_nodes) {
        node.second.show_info();
    }
}

const Node * FBDGraph::get_node(int local_id) const {
    auto it = m_nodes.find(local_id);
    if (it != m_nodes.end()) {
        return &it->second;
    }

    return nullptr;
}

} // namespace plctonusm


