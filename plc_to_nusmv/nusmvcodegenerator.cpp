/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include "nusmvcodegenerator.h"

namespace plctonusmv {

void NusmvCodeGenerator::visit(const Program * node)
{
    using std::endl;

    auto module_list = node->get_module_list();
    auto it = module_list.begin();
    (*it)->accept(*this);
    it++;
    os << endl;

    for (; it != module_list.end(); ++it) {
        for (int i = 0; i < 80; i++) {
            os << "-";
        }
        os << endl;
        (*it)->accept(*this);
        os << endl;
    }
}

void NusmvCodeGenerator::visit(const Module * node)
{
    using std::endl;

    std::string original_indent = indent;
    indent = indent + "    ";

    // Print name and parameters
    auto params = node->get_params();
    os << "MODULE " << node->get_name();
    if (params.size() > 0) {
        os << "(";
        auto it = params.begin();
        it->accept(*this);
        ++it;
        for (; it != params.end(); ++it) {
            os << ", ";
            it->accept(*this);
        }
        os << ")";
    }

    os << endl;

    // Print variable declerations
    auto vars = node->get_vars();
    if (vars.size() > 0) {
        os << "VAR" << endl;
        for (const VarDecl & d : vars) {
            d.accept(*this);
            os << endl;
        }
    }


    // Print variable declerations
    auto defines = node->get_defines();
    if (defines.size() > 0) {
        os << "DEFINE" << endl;
        for (const DefineDecl & d : defines) {
            d.accept(*this);
            os << endl;
        }
    }

    // Print variable declerations
    auto assigns = node->get_assigns();
    if (assigns.size() > 0) {
        os << "ASSIGN" << endl;
        for (const AssignDecl & d : assigns) {
            d.accept(*this);
            os << endl;
        }
    }

    // Print LTL specifications
    auto ltl_specs = node->get_ltl_specs();
    for (const LTLExpr & ltl : ltl_specs) {
        os << "LTLSPEC ";
        ltl.accept(*this);
        os << endl;
    }

    indent = original_indent;
}

// Declerations
void NusmvCodeGenerator::visit(const ParamDecl * node)
{
    os << node->get_name();
}

void NusmvCodeGenerator::visit(const VarDecl * node)
{
    using std::endl;

    os << indent << node->get_name() << " : ";
    const TypeSpecifier * type = node->get_type_specifier();
    type->accept(*this);
    os << ";";
}

void NusmvCodeGenerator::visit(const DefineDecl * node)
{
    using std::endl;

    os << indent << node->get_identifier() << " := ";
    const Expression * expr = node->get_expr();
    expr->accept(*this);
    os << ";";
}

void NusmvCodeGenerator::visit(const AssignDecl * node)
{
    using std::endl;

    std::string original_indent = indent;
    indent = indent + "    ";

    os << original_indent;

    AssignDecl::assign_type_enum assign_type = node->get_assign_type();
    if (assign_type == AssignDecl::INIT) {
        os << "init(";
    } else if (assign_type == AssignDecl::NEXT) {
        os << "next(";
    }

    const VarExpr * var = node->get_var();
    var->accept(*this);

    if (assign_type == AssignDecl::INIT || assign_type == AssignDecl::NEXT)
        os << ")";

    os << " := ";

    const Expression * expr = node->get_expr();
    expr->accept(*this);
    os << ";";

    indent = original_indent;
}

void NusmvCodeGenerator::visit(const TypeSpecifier * node)
{
    using std::endl;

    os << node->get_type();
    auto params = node->get_params();
    if (params.size() > 0) {
        os << "(";
        auto it = params.begin();
        (*it)->accept(*this);
        it++;

        for (; it != params.end(); ++it) {
            os << ", ";
            (*it)->accept(*this);
        }

        os << ")";
    }
}

// Expressions
void NusmvCodeGenerator::visit(const VarExpr * node)
{
    using std::endl;

    os << node->get_identifier();
    const VarExpr * var = node->get_var();
    if (node->is_expr_set()) {
        os << ".";
        var->accept(*this);
    }
}

void NusmvCodeGenerator::visit(const LiteralExpr * node)
{
    (void)indent;
    os << node->get_literal();
}

void NusmvCodeGenerator::visit(const OpExpr * node)
{
    using std::endl;

    const Expression * left = node->get_left_expression();
    const Expression * right = node->get_right_expression();

    bool paranthesize_left = true;

    if (dynamic_cast<const VarExpr*>(left))
        paranthesize_left = false;

    if (paranthesize_left)
        os << "(";

    if (! right)
        os << node->get_symbol();

    left->accept(*this);

    if (paranthesize_left)
        os << ")";

    if (right) {
        bool paranthesize_right = ! dynamic_cast<const VarExpr*>(right);

        os << " " << node->get_symbol() << " ";
        if (paranthesize_right)
            os << "(";
        right->accept(*this);
        if (paranthesize_right)
            os << ")";
    }
}

void NusmvCodeGenerator::visit(const CaseExpr * node)
{
    using std::endl;

    std::string original_indent = indent;
    indent = indent + "    ";

    os << endl;
    os << original_indent << "case" << endl;

    if (node->is_truth_table_set()) {
        auto truth_table = node->get_truth_table();
        auto tt_vars = node->get_truth_table_vars();
        for (const std::vector<bool> & tt_row : truth_table) {
            os << indent;

            size_t tt_col_index = 0;
            for (bool tt_cell_val : tt_row) {
                if (tt_cell_val)
                    os << tt_vars.at(tt_col_index);
                else
                    os << "!" << tt_vars.at(tt_col_index);
                tt_col_index++;

                if (tt_col_index < tt_vars.size())
                    os << " & ";
            }

            os << " : ";
            std::string tt_value = node->get_truth_table_value() ? "TRUE"
                                                                 : "FALSE";
            os << tt_value;
            os << ";";
            os << endl;
        }

        os << indent << "TRUE : ";
        if (node->get_truth_table_value())
            os << "FALSE;";
        else
            os << "TRUE;";
        os << endl;
    } else {
        auto cases = node->get_cases();
        for (const std::pair<Expression *, Expression *> & c : cases) {
            os << indent;
            c.first->accept(*this);
            os << " : ";
            c.second->accept(*this);
            os << ";";
            os << endl;
        }
    }
    os.flush();

    indent = original_indent;
    os << indent << "esac";
}

void NusmvCodeGenerator::visit(const SetExpr * node)
{
    using std::endl;

    os << "{";
    auto expressions = node->get_expressions();
    auto it = expressions.begin();
    (*it)->accept(*this);
    it++;

    for (; it != expressions.end(); ++it) {
        os << ", ";
        (*it)->accept(*this);
    }

    os << "}";
}

void NusmvCodeGenerator::visit(const LTLExpr * node)
{
    using std::endl;

    std::string name = node->get_name();
    std::string symbol = node->get_symbol();

    if (name.size() > 0)
        os << "NAME " << name << " := ";


    const Expression * left = node->get_left_expression();
    const Expression * right = node->get_right_expression();

    if (! right && symbol.size() > 0)
        os << symbol << " ";

    if (!is_ltl_symbol(symbol))
        os << "(";

    left->accept(*this);

    if (right) {
        os << " " << symbol;
        if (is_ltl_symbol(symbol)) {
            os << endl;
            os << indent;
        } else {
            os << " ";
        }

        right->accept(*this);
    }

    if (!is_ltl_symbol(symbol))
        os << ")";
}

bool NusmvCodeGenerator::is_ltl_symbol(const std::string & symbol) {
    return (symbol == "&" || symbol == "|" || symbol == "xor");
}

} // namespace plctonusm
