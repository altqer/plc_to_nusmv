/*
PLC-NuSMV compiler
Copyright (C) 2016  Altin Qeriqi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact information:
Contact Altin Qeriqi at altin89@gmail.com
*/


#include "plctonusmvlogger.h"

namespace plctonusmv {

void PlcToNusmvLogger::warn(const std::string &msg) {
    m_os << "WARNING: " << msg << std::endl;
}

void PlcToNusmvLogger::error(const std::string &msg) {
    m_os << "ERROR: " << msg << std::endl;
}

void PlcToNusmvLogger::fatal_error(const std::string &msg) {
    m_os << "FATAL ERROR: " << msg << std::endl
         << "Aborting." << std::endl;
}

} // namespace plctonusm
